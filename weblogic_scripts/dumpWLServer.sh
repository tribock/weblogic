###
# create threadDump
###

#fill in variables

component=$1
echo "component: ${component}"

domPrefix=$2
echo "domPrefix: ${domPrefix}"

javaBin="/u01/app/oracle/product/`/u01/common/general/scripts/values/domainValues.sh value ${domPrefix} activJavaVersion only`/jdk_${domPrefix,,}/bin"
echo "javaBin .: ${javaBin}"

#count amount of processes containing component
if [ `ps -ef | grep ${component} | grep java | grep ${domPrefix} | wc -l` == 1 ]
  then
    #get pid of
    wlsPID=`pgrep -fl ${component} | grep java | cut -d " " -f 1`
    echo ${wlsPID} " is pid"
    export dumpFileName=/u01/common/general/dumps/${component}-$(date +"%Y%m%d_%H%M")-${wlsPID}

    #write procinfo into file
    ps axf | grep ${wlsPID} | grep -v grep>${dumpFileName}.procinfo

    for (( i=1; i <= 6; i++ ))
    do
      echo "Generating Thread Dump ${i} on '`hostname`'"
      echo "cmd: ${javaBin}/jstack ${wlsPID} > ${dumpFileName}-0${i}.tdump"
      ${javaBin}/jstack ${wlsPID} > ${dumpFileName}-0${i}.tdump
      if [[ $(( i % 3 )) == 0 ]]
        then
          echo "Generating Heap Dump ${i}"
          echo "cmd: ${javaBin}/jmap -dump:format=b,file=${dumpFileName}-0${i}.hprof ${wlsPID}"
          ${javaBin}/jmap -dump:format=b,file=${dumpFileName}-0${i}.hprof ${wlsPID}
      fi
    done
  else
    echo "there is not exactly one Process running on host '`hostname`''. Please change param."
    exit 1
fi