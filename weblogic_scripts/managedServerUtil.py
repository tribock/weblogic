def showServerStatus ():
  domainRuntime();
  servers = [];
  for server in cmo.getServerLifeCycleRuntimes():
    servers.append(server.getName() + '\t\t' + server.getState())
  servers.sort()
  print "\n".join([str(x) for x in servers] )

def waitTillCompleted (tasks):
  returncode=0
  if not tasks:
    print "no task to execute!"
    print "-> the component is already in the target status"
    exit(exitcode=returncode)

  while len(tasks) > 0:
    java.lang.Thread.sleep(3000)
    for task in tasks:
      if task.getStatus() == 'FAILED':
        print "operation failed!"
        returncode=1
        tasks.remove(task)
      elif task.getStatus()  != 'TASK IN PROGRESS' :
        tasks.remove(task)
  showServerStatus ()
  exit(exitcode=returncode)

def setFilter(component):
  serverFilter='something that should not work!'
  domainConfig();
  if (component == "all"):
    serverFilter=""
  elif ("Cluster" in component):
    for cluster in cmo.getClusters():
      if (cluster.getName() in component):
        serverFilter=cluster.getName()
        serverFilter=string.replace(serverFilter, 'Cluster', 'Server')
  elif ("Server" in component):
    serverFilter=component
  elif (component == 'even' or component == 'odd'):
    return serverFilter
  else:
    print "wrong Component!"
    exit(exitcode=1)
  return serverFilter