from java.io import File
from java.io import FileOutputStream

def connectToAdminServer(home, domName, admsrvHost, admsrvPort):
  prev = suppressDefaultMessages()
  try:
    connect(userConfigFile=home + '/.WLST/' + domName + '/weblogicConfigFile', userKeyFile=home + '/.WLST/' + domName + '/weblogicKeyFile', url="t3://" + admsrvHost + ":" + admsrvPort)
  except Exception, err:
    enableDefaultMessages(prev)
    if 'old_stdout' in locals() or 'old_stdout' in globals():
      sys.stdout = old_stdout
    print "connection to AdminServer of " + domName + " failed!"
    print err
    exit(exitcode=1)
  enableDefaultMessages(prev)

def connectToNodeManager(home, domName, admsrvHost, ndmgrPort):  
  prev = suppressDefaultMessages()
  try:
    nmConnect(userConfigFile=home + '/.WLST/' + domName + '/nodemanagerConfigFile', userKeyFile=home + '/.WLST/' + domName + '/nodemanagerKeyFile', host=admsrvHost, port=ndmgrPort, domainName=domName , domainDir='/u01/app/oracle/domains/' + domName + '', nmType='plain')
  except Exception, err:
    enableDefaultMessages(prev)
    print err
    exit(exitcode=1)
  enableDefaultMessages(prev)

def suppressDefaultMessages():
  prev = theInterpreter.getOut()
  f = File("/dev/null")
  fos = FileOutputStream(f)
  theInterpreter.setOut(fos)
  return prev

def enableDefaultMessages(prev):
  theInterpreter.setOut(prev)