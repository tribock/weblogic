#!/bin/env /bin/bash
###
#  main script for weblogic, calls wlst with py-scripts    
###


#
# Debugging ...
#
echoerr() { echo "$@" 1>&2; }

export dbgCmd="echo"     # Debugausgaben werden in stdout gemacht ...
export dbgCmd="echoerr"  # Debugausgaben werden in stderr gemacht ...
export dbgCmd=":"        # Keine Debugausgaben ...
                                                                                                  ${dbgCmd} "+++ Beginn $0 PID: $$"


#
# Save call-parameter
#
prefix=${1:-help}
cmd=${2}
component=${3:-all}

  
#
# Interne Funktionen
#
f_chkDomain()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_chkDomain"
                                                                                                  ${dbgCmd} "+++ Call-Parameter1: '${1}' (Domain-Prefix)"
    #
    # Ist die Domain Aktiv?
    #
    typeset -l l_domainActiv=`${c_domainValue} value ${1} activDomain only`
                                                                                                  ${dbgCmd} "+++ l_domainActiv  : ${l_domainActiv}"
    if [ "${l_domainActiv}" != "ja" ]
    then
        l_domKeyActiv=""

        for y in `${c_domainValue} fieldvalues activDomain | grep -i ja | awk '{ print $1 }'`
        do
            l_domKeyActiv="${l_domKeyActiv}${y}|"
        done

        echo "ERROR: Unknown or inactiv domain '${1}'"
        echo "       use one domain of: {${l_domKeyActiv}help}"
        exit 1
    fi
                                                                                                  ${dbgCmd} "<<< Funktion : f_chkDomain"
}

f_setBlackOut()
{
   l_doOEM=`${c_domainValue} value ${1} oemMonitoring only`

   if [ "${l_doOEM,,}" == "ja" ]
   then
      echo "Set BlackOut for ${2} ..."
      /u01/common/general/scripts/wls/oem.sh ${1} set ${2}
   fi
}

f_releaseBlackOut()
{
   l_doOEM=`${c_domainValue} value ${1} oemMonitoring only`

   if [ "${l_doOEM,,}" == "ja" ]
   then
      echo "Release BlackOut for ${2} ..."
      /u01/common/general/scripts/wls/oem.sh ${1} release ${2}
   fi
}

f_chkAdminServer()
{
   export prefix_u=`echo ${prefix} | tr  [a-z] [A-Z]`
   
   l_environment=`${c_domainValue} value ${prefix} Environment  only`
 
   if [ "${l_environment,,}" == "exa" ]
   then

      /u01/common/general/scripts/all_cn/allcn_do.sh "ps -ef  | grep   ${prefix_u:0:1}0[0-9]_AdminServer" | grep "^cn0[0-9]" >/var/tmp/wls_${prefix}.wrk
       
      export adminActualOn=`cat /var/tmp/wls_${prefix}.wrk | grep     "${prefix_u}_AdminServer " | cut -f1 -d":" | cut -f1 -d"-"`
      export adminOtherOn=`cat /var/tmp/wls_${prefix}.wrk  | grep -v  "${adminActualOn}" | grep "${prefix_u:0:1}0[0-9]_AdminServer" | tail -1 | cut -f1 -d":" | cut -f1 -d"-"`
      export adminCnt=`cat /var/tmp/wls_${prefix}.wrk      | grep -ic "${prefix_u}_AdminServer "`
      export adminCntAll=`cat /var/tmp/wls_${prefix}.wrk   | grep -ic "${prefix_u:0:1}0[0-9]_AdminServer "`
    
      export adminActualOn=${HOSTNAME:0:4}${adminActualOn}
      export adminOtherOn=${HOSTNAME:0:4}${adminOtherOn}
   else
      export adminCnt=`ps -ef | grep -v grep | grep -ci ${prefix_u}_AdminServer`
      if [ ${adminCnt} -gt 0 ]
      then
         export adminActualOn=${HOSTNAME}
      else
         export adminActualOn=""
      fi
      export adminOtherOn=""
   fi

#    echo "adminActualOn ${adminActualOn}" &1>&2
#    echo "adminOtherOn  ${adminOtherOn}"  &1>&2
#    echo "adminCnt ${adminCnt}"           &1>&2

   case ${adminCnt} in
      0 )   if [[ "$cmd" == "admjojo" || "$cmd" == "admrestart" || "$cmd" == "admup" ]]
            then
               return
            fi
            
            echo "****************************************"         &1>&2
            echo "*** Keinen laufenden ${prefix_u}_AdminServer ***" &1>&2
            echo "*** gefunden.                        ***"         &1>&2
            echo "****************************************"         &1>&2
            exit 0
      ;;
      
      1 )   if [[ "$cmd" == "admjojo" || "$cmd" == "admrestart" || "$cmd" == "admdown" ]]
            then
               return
            fi
            
            if [[ "$cmd" == "admup" && "${HOSTNAME}" == "${adminActualOn}" ]]
            then
               echo "******************************************"         &1>&2
               echo "*** Der Adminserver ${prefix_u}_AdminServer    ***" &1>&2
               echo "*** läuft bereits.                     ***"         &1>&2
               echo "******************************************"         &1>&2
               exit 1
            else
               echo "******************************************"         &1>&2
               echo "*** Der Adminserver ${prefix_u}_AdminServer    ***" &1>&2
               echo "*** läuft bereits auf '${adminActualOn}'.      ***" &1>&2
               echo "******************************************"         &1>&2
               exit 1
            fi
      ;;
      
      * )   if [[ "$cmd" == "admjojo" || "$cmd" == "admrestart" ]]
            then
               echo "**********************************************"               &1>&2
               echo "*** Es läuft bereits ein ${prefix_u:0:1}0<n>_AdminServer ***" &1>&2
               echo "*** läuft auf '${adminOtherOn}'. Starten Sie den  ***"        &1>&2
               echo "*** Befehl nochmals auf '${adminOtherOn}'         ***"        &1>&2
               echo "**********************************************"               &1>&2
               exit 1
            fi
      ;;
   esac            
}


#
# Ausgabe Help-Text
#
if [[ "${prefix}" == "help" || "${cmd}" == "help" ]]
then
   cat $(dirname "$0")/wls/wls_usage.txt
   exit 0
fi


export c_domainValue=/u01/common/general/scripts/values/domainValues.sh

f_chkDomain ${prefix}


#
# set local skript-parameter from "domainValues.csv" and more
#
domainName=`${c_domainValue} value ${prefix} domainName only` 
domainPrefix=`${c_domainValue} value ${prefix} domainPrefix only` 
ndmgrPort=`${c_domainValue} value ${prefix} nmPort only` 
admsrvHost=`${c_domainValue} value ${prefix} admsrvHost only` 
admsrvPort=`${c_domainValue} value ${prefix} admsrvPort only` 
environment=`${c_domainValue} value ${prefix} Environment only` 

if [ "${environment}" ==  "exa" ]
then
   hostname="cn${HOSTNAME: -2}-priv"
   eval ndmgrHome=`${c_domainValue} value ${prefix} cn${HOSTNAME: -2}NdmgrHome only` 
else
   hostname=localhost
   eval ndmgrHome=`${c_domainValue} value ${prefix} NdmgrHome only` 
fi

domainDir=/u01/app/oracle/domains/${domainName}
scriptDir=${domainDir}/scripts/wls

JAVA_OPTIONS=''

source ${domainDir}/bin/setDomainEnv.sh > /dev/null 2>&1

# Create wlsttmp Directory
l_mkdir="${domainDir}/wlsttmp"
if [ ! -d ${l_mkdir} ]
then
   l_cmd="mkdir -p ${l_mkdir}"
   echo "${l_cmd}"
   ${l_cmd}
fi


wlstCmd=${MW_HOME}/oracle_common/common/bin/wlst.sh
PROXY_SETTINGS=''

# switch to corresponding command
case "$cmd" in
    admup) 
        echo "Starting Adminconsole"
        f_chkAdminServer
        if [[ ${adminCnt} -eq 0 ]]
        then
           /u01/common/general/scripts/vip/vip.sh ${prefix} adminup
        fi
        l_cmd="${wlstCmd} ${scriptDir}/wls/adminUp.py ${domainName} ${domainPrefix} ${hostname} ${ndmgrPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        
        f_releaseBlackOut ${prefix} ${domainPrefix}_AdminServer
        ;;
    admdown) 
        echo "Shutting down Adminconsole"
        f_chkAdminServer
        f_setBlackOut ${prefix} ${domainPrefix}_AdminServer

        l_cmd="${wlstCmd} ${scriptDir}/wls/adminDown.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        
        if [[ ${adminCnt} -eq 1 && ${adminCntAll} -eq 1 ]]
        then
           /u01/common/general/scripts/vip/vip.sh ${prefix} admindown
        fi
        ;;
    admjojo | admrestart) 
        echo "Restart Adminconsole"
        f_chkAdminServer
        f_setBlackOut ${prefix} ${domainPrefix}_AdminServer

        if [[ ${adminCnt} -ne 0 ]]
        then
           l_cmd="${wlstCmd} ${scriptDir}/wls/adminDown.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
           ${l_cmd}
        fi 
        /u01/common/general/scripts/vip/vip.sh ${prefix} adminup
        l_cmd="${wlstCmd} ${scriptDir}/wls/adminUp.py ${domainName} ${domainPrefix} ${hostname} ${ndmgrPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}

        f_releaseBlackOut ${prefix} ${domainPrefix}_AdminServer
        ;;
    up)
        echo "Starting " ${component}
        l_cmd="${wlstCmd} ${scriptDir}/wls/serverUp.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${component} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}

        f_releaseBlackOut ${prefix} "${component}"
         ;;
    down)
        echo "Shutting down "${component}
        f_setBlackOut ${prefix} "${component}"
        
        l_cmd="${wlstCmd} ${scriptDir}/wls/serverDown.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${component} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        ;;
    jojo | restart)
        echo "Restart "${component}
        f_setBlackOut ${prefix} "${component}"

        l_cmd="${wlstCmd} ${scriptDir}/wls/serverDown.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${component} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        l_cmd="${wlstCmd} ${scriptDir}/wls/serverUp.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${component} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}

        f_releaseBlackOut ${prefix} "${component}"
       ;;
    ndmgrup) 
        echo "Starting Nodemanager"
        l_cmd="${wlstCmd} ${scriptDir}/wls/ndmgrUp.py ${hostname} ${ndmgrPort} ${ndmgrHome}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        ;;
    ndmgrdown) 
        echo "Shutting down Nodemanager"
        l_cmd="${wlstCmd} ${scriptDir}/wls/ndmgrDown.py ${domainName} ${domainPrefix} ${hostname} ${ndmgrPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        ;;
    ndmgrjojo | ndmgrrestart) 
        l_cmd="${wlstCmd} ${scriptDir}/wls/ndmgrDown.py ${domainName} ${domainPrefix} ${hostname} ${ndmgrPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        l_cmd="${wlstCmd} ${scriptDir}/wls/ndmgrUp.py ${hostname} ${ndmgrPort} ${ndmgrHome}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        ;;    
    stat)
        echo "Show status"
        l_cmd="${wlstCmd} ${scriptDir}/wls/stat.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        ;;
    dump)
        sshHost="cn0${component: -1}-priv"
        echo "Create dump on '${sshHost}'"

        l_cmd="${scriptDir}/dumpWLServer.sh ${component}  ${domainPrefix}"
                                                                                                  ${dbgCmd} "+++ cmd: ssh ${sshHost} '${l_cmd}'"
        ssh ${sshHost} "${l_cmd}"
        ;;
    *)   
        cat $(dirname "$0")/wls/wls_usage.txt
        exit 0
        ;;
esac
ret=$?
if [ $ret -ne 0 ]; then
     echo "Handle failure: " ${ret}
     exit 1
fi
