#!/bin/env /bin/bash
###
#  main script for weblogic, calls wlst with py-scripts    
###


#
# Debugging ...
#
echoerr() { echo "$@" 1>&2; }

export dbgCmd="echo"     # Debugausgaben werden in stdout gemacht ...
export dbgCmd="echoerr"  # Debugausgaben werden in stderr gemacht ...
export dbgCmd=":"        # Keine Debugausgaben ...
                                                                                                  ${dbgCmd} "+++ Beginn $0 PID: $$"


#
# Save call-parameter
#
prefix=${1:-help}
cmd=${2}
arg1=${3}
arg2=${4}
arg3=${5}
arg4=${6}
arg5=${7}

#
# interne Fuktionen
#
f_chkDomain()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_chkDomain"
                                                                                                  ${dbgCmd} "+++ Call-Parameter1: '${1}' (Domain-Prefix)"                                                       

    #
    # Ist die Domain Aktiv?
    #
    typeset -l l_domainActiv=`${c_domainValue} value ${1} activDomain only`
                                                                                                  ${dbgCmd} "+++ l_domainActiv  : ${l_domainActiv}"
    if [ "${l_domainActiv}" != "ja" ]
    then
        l_domKeyActiv=""

        for y in `${c_domainValue} fieldvalues activDomain | grep -i ja | awk '{ print $1 }'`
        do
            l_domKeyActiv="${l_domKeyActiv}${y}|"
        done

        echo "ERROR: Unknown or inactiv domain '${1}'"
        echo "       use one domain of: {${l_domKeyActiv}help}"
        exit 1
    fi
                                                                                                  ${dbgCmd} "<<< Funktion : f_chkDomain"
}



#
# Ausgabe Help-Text
#
if [[ "${prefix}" == "help" || "${cmd}" == "help" ]]
then
   cat $(dirname "$0")/wlsapps/wlsapps_usage.txt
   exit 0
fi

export c_domainValue=/u01/common/general/scripts/values/domainValues.sh

f_chkDomain ${prefix} 

#
# set local skript-parameter from "domainValues.csv"
#

domainName=`${c_domainValue} value ${prefix} domainName only` 
domainPrefix=`${c_domainValue} value ${prefix} domainPrefix only` 
admsrvHost=`${c_domainValue} value ${prefix} admsrvHost only` 
admsrvPort=`${c_domainValue} value ${prefix} admsrvPort only`
Environment=`${c_domainValue} value ${prefix} Environment only` 

domainDir=/u01/app/oracle/domains/${domainName}
scriptDir=${domainDir}/scripts/wls
                                                                                                
source ${scriptDir}/wlsapps/wlsapps_wlst_caller.sh 

component=${3-all}

JAVA_OPTIONS=''
source ${domainDir}/bin/setDomainEnv.sh > /dev/null 2>&1

wlstCmd=${MW_HOME}/oracle_common/common/bin/wlst.sh

PROXY_SETTINGS=''

if [[ -f "${arg1}" ]] 
then
  deployFile="${arg1}"
  cnt=`egrep -hw 'APP|LIB' ${deployFile} | wc -l`
  if [[ ${cnt} != 0 ]] 
  then
       cp ${deployFile} "/tmp/${prefix}_Deploy.dat"
  else
       echo "keine APP oder LIB Records in Inputfile gefunden"
  fi
else
  
  case $cmd in
    deployapp | redeployapp)
       deploymentName="${arg1}"
       targets="${arg2}"
       deploymentFile="${arg3}"
       planFile=""
       deploymentPrio=""
       
       if [[ -f ${arg4} && "${arg4}" == *"lan"*".xml" ]]
       then planFile="${arg4}"
       else
         if [[ ${arg4} -gt 0 && ${arg4} -lt 999 ]]
         then deploymentPrio="${arg4}"      
         fi
       fi

       if [[ ${arg5} -gt 0 && ${arg5} -lt 999 ]]
         then deploymentPrio="${arg5}"
       fi
       
       line="${prefix,,} | APP | ${targets} | ${deploymentName} | ${deploymentFile} | ${planFile} | ${deploymentPrio} | "
    ;;
    deploylib | redeploylib)
       deploymentName="${arg1}"
       targets="${arg2}"
       deploymentFile="${arg3}"
       specVers="${arg4}"
       implemVers="${arg5}"
       
       line="${prefix,,} | LIB | ${targets} | ${deploymentName}#${specVers}@${implemVers} | ${deploymentFile} | "
    ;;
    *)
    ;;
  esac
  echo -n "${line}" > /tmp/${prefix}_Deploy.dat
  
fi

case $cmd in
  deployapp | redeployapp | deploylib | redeploylib )       
        l_cmd="${wlstCmd} ${scriptDir}/wlsconfig/deployApp.py ${prefix} ${domainName} ${admsrvHost} ${admsrvPort} ${Environment}"
        ${l_cmd}

        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
        ;; 
  undeployapp)
        undeploylib
        ;;
  stopapp)
        stopapp
        ;;
  startapp)
        startapp
        ;;
  statusapp)
        statusapp
        ;;
  listapps)
        listapps
        ;;
  listlibs)
        listlibs
        ;;
  undeploylib)
        undeploylib
        ;;
  restartapp)
        stopapp
        startapp
        ;;
   *)
        cat $(dirname "$0")/wlsapps/wlsapps_usage.txt
        exit 0
        ;;
esac

exit $RETVAL
