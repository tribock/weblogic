###
# start Server/Cluster/Domain
###

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
components=sys.argv[5]
scriptDir=sys.argv[6]

import string
execfile(scriptDir + "/managedServerUtil.py")
execfile(scriptDir + "/connectionUtil.py")

#get user home
from os.path import expanduser
home=expanduser("~")

tasks=[]

#connect to Adminserver
connectToAdminServer(home, domName, admsrvHost, admsrvPort)

def startServer(server):
    print server.getName()
    try:
      tasks.append(server.start())
    except Exception, err:
      print "Startup of " + component + " failed!"
      print err
      exit(exitcode=1)

for component in components.split(","):
  serverFilter=setFilter(component)
  domainRuntime();
  for server in cmo.getServerLifeCycleRuntimes():
    if (server.getName() != domPrefix + '_AdminServer' and server.getState() != 'RUNNING'):
      if (serverFilter in server.getName() ):
        startServer(server)
      elif(component == 'even' and (server.getName().endswith('2') or server.getName().endswith('4'))):
        startServer(server)
      elif(component == 'odd' and (server.getName().endswith('1') or server.getName().endswith('3'))):
        startServer(server)

waitTillCompleted (tasks)