###
# show status of domain
###

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]

execfile(scriptDir + "/managedServerUtil.py")
execfile(scriptDir + "/connectionUtil.py")

#get user home
from os.path import expanduser
home = expanduser("~")

connectToAdminServer(home, domName, admsrvHost, admsrvPort)
showServerStatus ()
exit(exitcode=0)

