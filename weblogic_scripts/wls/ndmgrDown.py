###
# stop nodemanager
###

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
ndmgrPort=sys.argv[4]
scriptDir=sys.argv[5]

execfile(scriptDir + "/connectionUtil.py")

#get user home
from os.path import expanduser
home = expanduser("~")

connectToNodeManager(home, domName, admsrvHost, ndmgrPort)

try:
  stopNodeManager()
except Exception, err:
  print err
  exit(exitcode=1)

exit(exitcode=0)
