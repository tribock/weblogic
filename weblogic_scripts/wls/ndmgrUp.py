###
# start nodemanager
###

#fill in variables

ndmgrHost=sys.argv[1]
ndmgrPort=sys.argv[2]
ndmgrHome=sys.argv[3]

try:
  startNodeManager(verbose='true', NodeManagerHome=ndmgrHome, ListenPort=ndmgrPort, ListenAddress=ndmgrHost);
except Exception, err:
  print err
  exit(exitcode=1)
exit(exitcode=0)