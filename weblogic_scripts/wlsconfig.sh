#!/bin/bash
###
#  main script for weblogic, calls wlst with py-scripts
###


#
# Debugging ...
#
echoerr() { echo "$@" 1>&2; }

export dbgCmd="echo"     # Debugausgaben werden in stdout gemacht ...
export dbgCmd="echoerr"  # Debugausgaben werden in stderr gemacht ...
export dbgCmd=":"        # Keine Debugausgaben ...
                                                                                                  ${dbgCmd} "+++ Beginn $0 PID: $$"

#
# Save call-parameter
#
prefix=${1-help}
cmd=${2}
maxFileArg=${3-62}
serverName=${3}
configFile=${3}
AdminNetworkListenPort=${4}
HTTPSListenAddr=${5}


#
# Interne Funktionen
#
f_chkCallParam()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_chkCallParam"
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter1: '${prefix}' (prefix)"   
   f_chkDomain ${prefix}                                                                                                                                                      
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter2: '${cmd}' (cmd)" 
   f_chkCmd                                                                                                                                                     
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter3: '${maxFileArg}' (maxFileArg/serverName/configFile)"                                                       
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter4: '${AdminNetworkListenPort}' (AdminNetworkListenPort)"                                                       
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter5: '${HTTPSListenAddr}' (HTTPSListenAddr)"                                                       
   
                                                                                                  ${dbgCmd} "<<< Funktion : f_chkCallParam"
}

f_chkCmd()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_chkCmd"
   case ${cmd,,} in
      genproperties | listsecurityrealms | updatedomain | createdomain | installsw )
         echo "+++ cmd-Check: Command '${cmd}' is valid"
         ;;

      *)
         echo "+++ cmd-Check: Command '${cmd}' is NOT valid"
         cat $(dirname "$0")/wlsconfig/wlsconfig_usage.txt
         exit 0
         ;;         
   esac                                                                                           
                                                                                                  ${dbgCmd} "<<< Funktion : f_chkCmd"
}

f_chkDomain()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_chkDomain"
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter1: '${1}' (Domain-Prefix)"                                                       
    #
    # Ist die Domain Aktiv?
    #
    typeset -l l_domainActiv=`${c_domainValue} value ${1} activDomain only`
                                                                                                  ${dbgCmd} ".   +++ l_domainActiv  : ${l_domainActiv}"
    if [ "${l_domainActiv}" != "ja" ]
    then
        l_domKeyActiv=""

        for y in `${c_domainValue} fieldvalues activDomain | grep -i ja | awk '{ print $1 }'`
        do
            l_domKeyActiv="${l_domKeyActiv}${y}|"
        done

        echo "ERROR: Unknown or inactiv domain '${1}'"
        echo "       use one domain of: {${l_domKeyActiv}help}"
        exit 1
    fi
                                                                                                  ${dbgCmd} "<<< Funktion : f_chkDomain"
}

f_updateDomain()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_updateDomain"
   if [[ ! -f ${domainDir}/config/config.xml ]]
   then
      echo "ERROR: The domain directory '${domainName}' not found!"     2>&1 | tee -a ${logFile}
      exit 1
   fi

   echo "update existing domain ${domainName}"                          2>&1 | tee -a ${logFile}
   
   rm -f /tmp/${prefix}_Config.dat                                   2>&1 | tee -a ${logFile}
   if [ $? -ne 0 ]
   then 
      echo " - ERROR: can not remove file '/tmp/${prefix}_Config.dat'"     2>&1 | tee -a ${logFile}
      exit 1
   fi 

   
   if [[ ! -f ${configFile} && "${configFile}" == "" ]]
   then
      echo " - 'configFile' not defined, create '/tmp/${prefix}_Config.dat' with '${prefix}'-records"  2>&1 | tee -a ${logFile}
            
      grep -h ^${prefix} /orasrc/scripts/dbtx/tmp/*Config.dat >/tmp/${prefix}_Config.dat 
   else
      if [[ "${configFile}" == "/"*".dat" ]]
      then      
         grep -hi ^${prefix} ${configFile} >/tmp/${prefix}_Config.dat

         l_inpCnt=`grep -v ^# ${configFile}        | wc -l | awk '{print $1}'`
         l_outCnt=`wc -l /tmp/${prefix}_Config.dat         | awk '{print $1}'`
                                                                                                  ${dbgCmd} ".   +++ l_inpCnt: '${l_inpCnt}'"
                                                                                                  ${dbgCmd} ".   +++ l_outCnt: '${l_outCnt}'"
         if [[ ${l_inpCnt} -ne ${l_outCnt} ]]
         then
            echo " - ERROR: other records then comment (#) and '${prefix}'-prefix in the File '${configFile}' found"  2>&1 | tee -a ${logFile}
            grep -vf /tmp/${prefix}_Config.dat ${configFile} | grep -v "^#" | awk '{print "   - Record: <" $0 ">"}'
            exit 1
         fi

         echo " - copy '${configFile}' to  '/tmp/${prefix}_Config.dat'"    2>&1 | tee -a ${logFile}
      else
         echo " - ERRRO: The Filename '${configFile}' must be fully qualified (starting with '/' and ending with '.dat')"
         exit 1
      fi
   fi
   
   l_exitCd=0

   l_recCnt=`grep -vc " | $" /tmp/${prefix}_Config.dat`

   if [[ ${l_recCnt} -ne 0 ]]
   then
      echo " - ERROR: The following records in the File '${configFile}' dose not end with caracters ' | '."  2>&1 | tee -a ${logFile}
      grep -v " | $" ${configFile} | grep -v "^#" | awk '{print "   - Record: <" $0 ">"}'
      l_exitCd=1
   fi

   l_recCnt=`grep -c " | | " /tmp/${prefix}_Config.dat`

   if [[ ${l_recCnt} -ne 0 ]]
   then
      echo " - ERROR: The following records in the File '${configFile}' dose not end with caracters ' | '."  2>&1 | tee -a ${logFile}
      grep " | | " ${configFile} | grep -v "^#" | awk '{print "   - Record: <" $0 ">"}'
      l_exitCd=1
   fi

   l_recCnt=`grep -c "| DOM |" /tmp/${prefix}_Config.dat`
   if [[ ${l_recCnt} -eq 0 ]]
   then
      echo " - ERROR: 'DOM'-Record missing in the File '${configFile}'."  2>&1 | tee -a ${logFile}
      l_exitCd=1
   fi

   l_recCnt=`grep -c "| ADM |" /tmp/${prefix}_Config.dat`
   if [[ ${l_recCnt} -eq 0 ]]
   then
      echo " - ERROR: 'ADM'-Record missing in the File '${configFile}'."  2>&1 | tee -a ${logFile}
      l_exitCd=1
   fi

   l_recCnt=`grep "| PRO |" /tmp/${prefix}_Config.dat | grep -c "| DefaultAuthenticator |"`
   if [[ ${l_recCnt} -eq 0 ]]
   then
      echo " - ERROR: 'PRO'-Record for 'DefaultAuthenticator' missing in the File '${configFile}'."  2>&1 | tee -a ${logFile}
      l_exitCd=1
   fi

   if [[ ${l_exitCd} -ne 0 ]]
   then
      echo "          Add the missing record(s) for the ${prefix} domain or correct the wrong record(s) and restart wlsconfig"  2>&1 | tee -a ${logFile}
      exit 1
   fi
   

   echo "Use new '/tmp/${prefix}_Config.dat'-File with `wc -l /tmp/${prefix}_Config.dat | awk '{print $1}'` '${prefix}'-records" 2>&1 | tee -a ${logFile}

   l_cmd="${wlstCmd} ${originScriptDir}/wlsconfig/updateDomain.py ${prefix} ${wlsVersion} ${javaVersion} 2>&1"
                                                                                                  ${dbgCmd} ".   +++ cmd: '${l_cmd}'"
   ${l_cmd} | awk '{print (" | " $0) }' | tee -a ${logFile}
                                                                                                  ${dbgCmd} "<<< Funktion : f_updateDomain"
}

f_setUmask()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_setUmask"
                                                                                                  ${dbgCmd} ".   +++ Call-Parameter1: '${1}' (Verzeichnis)"                                                       
   if [ -d ${1} ] 
   then 
      cd  ${1}
      for l_FileName in `grep -r "^[ ]*umask [ ]*02[0-9]" . | grep -v "orig-20" | grep -v ":#" | grep -v 022$ | cut -f1 -d":"`
      do
         l_saveFile="${l_FileName}.orig-`date +"%Y%m%d_%H%M"`"
         cp ${l_FileName} ${l_saveFile}
         l_cmd="sed  '/[ ]*umask [ ]*02[0-9]/s/umask/#umask/g' ${l_saveFile} | sed '/#umask /a umask 022'  >${l_FileName}"
                                                                                                  ${dbgCmd} ".   +++ cmd: '${l_cmd}'"
         eval ${l_cmd}
      done
   fi      
                                                                                                  ${dbgCmd} "<<< Funktion : f_setUmask"
}

f_createDomain()
{
                                                                                                  ${dbgCmd} ">>> Funktion : f_createDomain"
                                                                                                  ${dbgCmd} ".   +++ configFile ....: '${configFile}'"
                                                                                                  ${dbgCmd} ".   +++ domainDir .....: '${domainDir}'"
                                                                                                  ${dbgCmd} ".   +++ domainLogDir ..: '${domainLogDir}'"
                                                                                                  ${dbgCmd} ".   +++ domainName ....: '${domainName}'"
                                                                                                  ${dbgCmd} ".   +++ domainPrefix ..: '${domainPrefix}'"
                                                                                                  ${dbgCmd} ".   +++ JAVA_HOME .....: '${JAVA_HOME}'"
                                                                                                  ${dbgCmd} ".   +++ javaVersion ...: '${javaVersion}'"
                                                                                                  ${dbgCmd} ".   +++ logFile .......: '${logFile}'"
                                                                                                  ${dbgCmd} ".   +++ MW_HOME .......: '${MW_HOME}'"
                                                                                                  ${dbgCmd} ".   +++ ndmgrPort .....: '${ndmgrPort}'"
                                                                                                  ${dbgCmd} ".   +++ originScriptDir: '${originScriptDir}'"
                                                                                                  ${dbgCmd} ".   +++ prefix ........: '${prefix}'"
                                                                                                  ${dbgCmd} ".   +++ wlstCmd .......: '${wlstCmd}'"
                                                                                                  ${dbgCmd} ".   +++ wlsVersion ....: '${wlsVersion}'"
                                                                                                  
   if [[ -f /u01/app/oracle/domains/${domainName}/config/config.xml ]]
   then
      echo "ERROR: The domain directory '${domainName}' is not empty!"   2>&1 | tee -a ${logFile}
      echo "       Delete the content in the directory"                  2>&1 | tee -a ${logFile}
      echo "       '/u01/app/oracle/domains/${domainName}' and start again."    2>&1 | tee -a ${logFile}
      echo "       --> Action cancelled"                                 2>&1 | tee -a ${logFile}
      exit 1
   fi
                                                                                                  ${dbgCmd} ".   +++ Chk-DomainDir done"
   source ${originScriptDir}/installsw.inc
                                                                                                  ${dbgCmd} ".   +++ 'source ${originScriptDir}/installsw.inc' done"

   echo "create new domain ${prefix}"                                    2>&1 | tee -a ${logFile}
   if [[ ! -f ${configFile} ]]
   then
      grep -h ${prefix} /orasrc/scripts/dbtx/tmp/*Config.dat >/tmp/${prefix}_Config.dat
      echo " - New '/tmp/${prefix}_Config.dat' with alle '${prefix}'-records created" 2>&1 | tee -a ${logFile}
   else
      cp ${configFile} /tmp/${prefix}_Config.dat
      echo " - copy '${configFile}' to  '/tmp/${prefix}_Config.dat'"     2>&1 | tee -a ${logFile}
   fi
   echo "Use the file '/tmp/${prefix}_Config.dat'"                       2>&1 | tee -a ${logFile}
   

   l_cmd="${wlstCmd} ${originScriptDir}/wlsconfig/createDomain.py ${prefix} ${ndmgrPort} ${wlsVersion} ${javaVersion} 2>&1"
   echo "${l_cmd}"
   ${l_cmd} | awk '{print (" | " $0) }' | tee -a ${logFile}

   # copy base-files ...
   l_mkdir="${domainLogDir}/${domainPrefix}_Nodemanager"
   if [ ! -d ${l_mkdir} ]
   then
      l_cmd="mkdir -p ${l_mkdir}"
      echo "${l_cmd}"
      ${l_cmd}
   fi  

   # 2019.02.01 - Nach Rücksprache mit Peter: Das DemoIdentity.jks wird nicht kopiert ...
   # l_cmd="cp -p  ${scriptDir}/wlsconfig/create/DemoIdentity.jks ${domainDir}/security/."
   # echo "${l_cmd}"
   # ${l_cmd}

   f_setUmask ${domainDir}/bin
   f_setUmask ${MW_HOME}/wlserver

   l_cmd="cp -p /u01/common/general/scripts/create/cacerts ${JAVA_HOME}/jre/lib/security/."
   echo "${l_cmd}"
   ${l_cmd}
   l_rcd=$?
   if [ ${l_rcd} -ne 0 ]
   then
      echo "### ERROR in script $0"
      echo "### CMD: '${l_cmd}' endet mit RCD: ${l_rcd}"
      exit 1
   fi

   l_cmd="cp -pr ${originScriptDir}/../../scripts  ${domainDir}/."
   echo "${l_cmd}"
   ${l_cmd}
   l_rcd=$?
   if [ ${l_rcd} -ne 0 ]
   then
      echo "### ERROR in script $0"
      echo "### CMD: '${l_cmd}' endet mit RCD: ${l_rcd}"
      exit 1
   fi

   l_cmd="cp -p  ${scriptDir}/wlsconfig/create/setUserOverrides.sh ${domainDir}/bin/."
   echo "${l_cmd}"
   ${l_cmd}
   l_rcd=$?
   if [ ${l_rcd} -ne 0 ]
   then
      echo "### ERROR in script $0"
      echo "### CMD: '${l_cmd}' endet mit RCD: ${l_rcd}"
      exit 1
   fi

   echo "+++ rename AdminServer to ${domainPrefix}_AdminServer"

   l_mkdir="${domainDir}/servers/${domainPrefix}_AdminServer/security"
   if [ ! -d ${l_mkdir} ]
   then
      l_cmd="mkdir -p ${l_mkdir}"
      echo "${l_cmd}"
      ${l_cmd}
   fi

   l_cmd="cp -pr ${domainDir}/servers/AdminServer/security ${domainDir}/servers/${domainPrefix}_AdminServer/."
   echo "${l_cmd}"
   ${l_cmd}
   l_rcd=$?
   if [ ${l_rcd} -ne 0 ]
   then
      echo "### ERROR in script $0"
      echo "### CMD: '${l_cmd}' endet mit RCD: ${l_rcd}"
      exit 1
   fi

   l_cmd="rm -rf ${domainDir}/servers/AdminServer"
   echo "${l_cmd}"
   ${l_cmd}

   # setDomainEnv bereinigen
   cd ${domainDir}/bin

   l_FileName='setDomainEnv.sh'
   l_cmd="cp -p ${l_FileName} ${l_FileName}.orig"
   echo "${l_cmd}"
   ${l_cmd}
   l_rcd=$?
   if [ ${l_rcd} -ne 0 ]
   then
      echo "### ERROR in script $0"
      echo "### CMD: '${l_cmd}' endet mit RCD: ${l_rcd}"
      exit 1
   fi

   sedcmd="s/SERVER_NAME=\"AdminServer\"/SERVER_NAME=\"${domainPrefix}_AdminServer\"/g"
                                                                                                  ${dbgCmd} ".   +++ sedcmd ........: '${sedcmd}'"
   sed  ${sedcmd} ${l_FileName}.orig  >${l_FileName}

   oracleHome=`cat /etc/passwd | grep "^oracle:"  | cut -f6 -d":"`
                                                                                                  ${dbgCmd} ".   +++ oracleHome ....: '${oracleHome}'"
   l_mkdir="${oracleHome}/.WLST/${domainName}"
   if [ ! -d ${l_mkdir} ]
   then
      l_cmd="mkdir -p ${l_mkdir}"
      echo "${l_cmd}"
      ${l_cmd}
   fi

   echo "for Starting the nodemanager ..."
   # echo "nohup /u01/common/general/scripts/wls/wls.sh ${prefix} ndmgrup &"
   echo "wls ${prefix} ndmgrup"

   echo "for Starting the adminserver ..."
   #echo "nohup /u01/app/oracle/domains/${domainName}/bin/startWebLogic.sh &"
   echo "wls ${prefix} admup"
                                                                                                  ${dbgCmd} "<<< Funktion : f_createDomain"
}

#
# Ausgabe Help-Text
#
if [[ "${prefix}" == "help" ]]; then
   cat $(dirname "$0")/wlsconfig/wlsconfig_usage.txt
   exit 0
fi

export c_domainValue=/u01/common/general/scripts/values/domainValues.sh

f_chkCallParam


#
# set local skript-parameter from "domainValues.csv" and more
#

admsrvHost=`${c_domainValue} value ${prefix} admsrvHost only`
admsrvPort=`${c_domainValue} value ${prefix} admsrvPort only`
domainName=`${c_domainValue} value ${prefix} domainName only`
domainPrefix=`${c_domainValue} value ${prefix} domainPrefix only`
Environment=`${c_domainValue} value ${prefix} Environment only`
javaVersion=`${c_domainValue} value ${prefix} activJavaVersion only`
fmwVersion=`${c_domainValue} value ${prefix} fmwVersion only`
ndmgrPort=`${c_domainValue} value ${prefix} nmPort only`
wlsVersion=`${c_domainValue} value ${prefix} activWlVersion only`

domainLogDir=/u01/app/oracle/logs/${domainName}
domainDir=/u01/app/oracle/domains/${domainName}
scriptDir=${domainDir}/scripts/wls

scriptDir=$(dirname "$0")
originScriptDir=${scriptDir}

                                                                                                  ${dbgCmd} "+++ domainLogDir: '${domainLogDir}'"
                                                                                                  ${dbgCmd} "+++ domainDir ..: '${domainDir}'"
# activate setDomainEnv when available
if [ "${cmd,,}" != "installsw" ]
then
                                                                                                  ${dbgCmd} "+++ if: '${cmd,,}' <> 'installsw' "
   if [ -f /u01/app/oracle/domains/${domainName}/bin/setDomainEnv.sh ]
   then
      source /u01/app/oracle/domains/${domainName}/bin/setDomainEnv.sh > /dev/null 2>&1
                                                                                                  ${dbgCmd} "+++ '/u01/app/oracle/domains/${domainName}/bin/setDomainEnv.sh' aktiviert"
   fi
fi

# create/initialize logfile ...
if [[ -d ${domainLogDir} ]]
then
   logDir=${domainLogDir}
else
   logDir=/var/tmp
fi
logDir=${logDir}/${domainPrefix}_wlsScripts
                                                                                                  ${dbgCmd} "+++ logDir .....: '${logDir}'"
if [ ! -d ${logDir} ]
then
   l_cmd="mkdir -p ${logDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
   ${l_cmd}
fi   
                                                                                                  ${dbgCmd} "+++ logDir '${logDir}' vorhanden"
# Load setDomainEnv if exists else, set variable manually
if [[ -f ${domainDir}/bin/setDomainEnv.sh && "${cmd,,}" != "installsw" ]]
then
                                                                                                  ${dbgCmd} "+++ if: '${domainDir}/bin/setDomainEnv.sh' gefunden"
   source ${domainDir}/bin/setDomainEnv.sh > /dev/null 2>&1
                                                                                                  ${dbgCmd} "+++ '/u01/app/oracle/domains/${domainName}/bin/setDomainEnv.sh' aktiviert"
   # Create wlsttmp Directory
   l_mkdir="${domainDir}/wlsttmp"
   if [ ! -d ${l_mkdir} ]
   then
      l_cmd="mkdir -p ${l_mkdir}"
      echo "${l_cmd}"
      ${l_cmd}
   fi
else
                                                                                                  ${dbgCmd} "+++ if: '${domainDir}/bin/setDomainEnv.sh' nicht gefunden"
   l_activWlVersion=`${c_domainValue} value ${prefix} activWlVersion only`
   export MW_HOME="/u01/app/oracle/product/${l_activWlVersion}/mwhome_${prefix,,}/wlserver/.."

   l_activJavaVersion=`${c_domainValue} value ${prefix} activJavaVersion only`
   export JAVA_HOME="/u01/app/oracle/product/${l_activJavaVersion}/jdk_${prefix,,}"
fi

wlstCmd=${MW_HOME}/oracle_common/common/bin/wlst.sh
                                                                                                  ${dbgCmd} "+++ MW_HOME .: '${MW_HOME}'"
                                                                                                  ${dbgCmd} "+++ JAVA_HOME: '${JAVA_HOME}'"
                                                                                                  ${dbgCmd} "+++ wlstCmd .: '${wlstCmd}'"

logFile=${logDir}/`basename $0 | cut -f1 -d"."`-${cmd}_`date +"%Y%m%d_%H%M%S"`.log

echo "$(date +'%d.%m.%Y %T') - Start new command"    >${logFile}
echo "******************************************"   >>${logFile}
echo "*** CMD: '$0 $@'"                             >>${logFile}
echo "******************************************"   >>${logFile}
echo "LogFile '${logFile}' created"


# switch to corresponding command
case "${cmd,,}" in

    genproperties)
                                                                                                  ${dbgCmd} "+++ admsrvHost .: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort .: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ domainName .: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix: '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ wlstCmd ....: '${wlstCmd}'"
         l_cmd="${wlstCmd} ${scriptDir}/wlsconfig/genBootStartupProps.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
         ${l_cmd}
         ;;

    listsecurityrealms)
                                                                                                  ${dbgCmd} "+++ admsrvHost .: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort .: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ domainName .: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix: '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ wlstCmd ....: '${wlstCmd}'"
         l_cmd="${wlstCmd} ${scriptDir}/wlsconfig/listUser_Group_Roles.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
         ${l_cmd}
         ;;

    updatedomain)
         f_updateDomain
         ;;

    createdomain)
         f_createDomain
         ;;

    installsw)
                                                                                                  ${dbgCmd} "+++ MW_HOME .......: '${MW_HOME}'"
                                                                                                  ${dbgCmd} "+++ originScriptDir: '${originScriptDir}'"
         source ${originScriptDir}/installsw.inc

         f_setUmask  ${MW_HOME}/wlserver
         ;;

    *)
         cat $(dirname "$0")/wlsconfig/wlsconfig_usage.txt
         exit 0
         ;;
esac
ret=$?
if [ $ret -ne 0 ]; then
     echo "Handle failure: " ${ret}
     exit 1
fi  