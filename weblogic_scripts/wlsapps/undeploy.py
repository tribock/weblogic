###
# undeploy Library/Deployment
###

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]
libName=sys.argv[6]


if len(sys.argv) > 7:
	targets=sys.argv[7]
else:
	targets=[]

execfile(scriptDir + "/connectionUtil.py")
#get user home
from os.path import expanduser
home = expanduser("~")

connectToAdminServer(home, domName, admsrvHost, admsrvPort)


#========================
#Application undeployment Section
#========================


try:
	print 'stopping and undeploying ..' +libName+ '\n'
	try: 
		stopApplication(libName)
	except:
		print 'not able to stop ' +libName+ '\n'
		exit(exitcode=1)
	edit()
	startEdit()
	if not targets:
		print 'removing ' +libName+ '\n'
		undeploy(libName)
	else:
		print 'removing ' + targets + ' from ' +libName+ '\n'
		undeploy(libName, targets=targets)
	activate()
except:
	print 'Error during the stop and undeployment of ' +libName+ '\n'
	exit(exitcode=1)