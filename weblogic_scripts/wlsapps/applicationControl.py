###
# control Library/Deployment
###

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]
libName=sys.argv[6]
cmd=sys.argv[7]


execfile(scriptDir + "/connectionUtil.py")
#get user home
from os.path import expanduser
home = expanduser("~")

connectToAdminServer(home, domName, admsrvHost, admsrvPort)


print 'performing ' + cmd + ' '+ libName + '\n'
try:
    if cmd == "stop":
        stopApplication(libName)
    else:
        startApplication(libName)
except:
    dumpStack()
    print
    'Error during ' + cmd + ' ' + libName + '\n'
    exit(1)

