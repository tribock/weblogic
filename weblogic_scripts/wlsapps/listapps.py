###
# list Librarys/Deployments
###
import sys, os

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]
listlibs=sys.argv[6]

execfile(scriptDir + "/connectionUtil.py")
#get user home
from os.path import expanduser
home = expanduser("~")
old_stdout = sys.stdout
sys.stdout = "something"

connectToAdminServer(home, domName, admsrvHost, admsrvPort)

outputlines = [];

if listlibs == "True":
    deployments = cmo.getLibraries()
    type="Libraries"
else:
    deployments = cmo.getAppDeployments()
    type = "AppDeployments"

for deployment in deployments:
    domainConfig()
    try:
      cd('/'+type+'/' + deployment.getName() + '/Targets')
    except:
      sys.stdout=old_stdout
      print '%52s %1s %25s %10s' % (deployment.getName(), ':', '\033[1;31m' + 'MISSING ENTRY' + '\033[0m', '(' + target + ')')
      sys.stdout = "something"
    else:
      wlstargets=ls(returnMap='true')
      domainRuntime()
      cd('AppRuntimeStateRuntime')
      cd('AppRuntimeStateRuntime')
      for target in wlstargets:
          deploymentName=deployment.getName()
          currentState=cmo.getCurrentState(deploymentName,target)
          if currentState == None:
              currentState = "UNDEFINED"
          sys.stdout=old_stdout
          if currentState == "STATE_ACTIVE":
              print '%52s %1s %25s %10s' % (deployment.getName(), ':', '\033[1;32m' + currentState + '\033[0m', '(' + target + ')')
          else:
              print '%52s %1s %25s %10s' % (deployment.getName(), ':', '\033[1;31m' + currentState + '\033[0m', '(' + target + ')')
          sys.stdout = "something"
