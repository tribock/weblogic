dbgCmd=${dbgCmd-:}

# deployapp() {
#        echo -e $"Deploying deployment in current domain (Domain ${domainName}):\n"
#        libraryModule="false"
#                                                                                                  ${dbgCmd} "+++ wlstCmd .....: '${wlstCmd}'"
#                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
#                                                                                                  ${dbgCmd} "+++ domainName ..: '${domainName}'"
#                                                                                                  ${dbgCmd} "+++ domainPrefix : '${domainPrefix}'"
#                                                                                                  ${dbgCmd} "+++ admsrvHost ..: '${admsrvHost}'"
#                                                                                                  ${dbgCmd} "+++ admsrvPort ..: '${admsrvPort}'"
#                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
#                                                                                                  ${dbgCmd} "+++ libraryModule: '${libraryModule}'"
#                                                                                                  ${dbgCmd} "+++ arg1 ........: '${arg1}'"
#                                                                                                  ${dbgCmd} "+++ arg2 ........: '${arg2}'"
#                                                                                                  ${dbgCmd} "+++ arg3 ........: '${arg3}'"
#                                                                                                  ${dbgCmd} "+++ arg4 ........: '${arg4}'"
#                                                                                                  ${dbgCmd} "+++ arg5 ........: '${arg5}'"
#        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/deployapp.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${libraryModule} ${arg1} ${arg2}  ${arg3} ${arg4} ${arg5}"
#                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
#        ${l_cmd}
#        RETVAL=$?
#        echo
#        [ $RETVAL -eq 0 ]
#}

undeploylib() {
        echo -e $"Undeploying library/deployment in current domain (Domain ${domainName}):\n"
                                                                                                  ${dbgCmd} "+++ wlstCmd .....: '${wlstCmd}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ domainName ..: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix : '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ admsrvHost ..: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort ..: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ arg1 ........: '${arg1}'"
                                                                                                  ${dbgCmd} "+++ arg2 ........: '${arg2}'"                                                                                                  
        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/undeploy.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${arg1} ${arg2}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
}

# deploylib() {
#        echo -e $"Deploying library in current domain (Domain ${domainName}):\n"
#        libraryModule="true"
#                                                                                                  ${dbgCmd} "+++ wlstCmd .....: '${wlstCmd}'"
#                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
#                                                                                                  ${dbgCmd} "+++ domainName ..: '${domainName}'"
#                                                                                                  ${dbgCmd} "+++ domainPrefix : '${domainPrefix}'"
#                                                                                                  ${dbgCmd} "+++ admsrvHost ..: '${admsrvHost}'"
#                                                                                                  ${dbgCmd} "+++ admsrvPort ..: '${admsrvPort}'"
#                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
#                                                                                                  ${dbgCmd} "+++ libraryModule: '${libraryModule}'"
#                                                                                                  ${dbgCmd} "+++ arg1 ........: '${arg1}'"
#                                                                                                  ${dbgCmd} "+++ arg2 ........: '${arg2}'"                                                                                                  
#                                                                                                  ${dbgCmd} "+++ arg3 ........: '${arg3}'"                                                                                                  
#                                                                                                  ${dbgCmd} "+++ arg4 ........: '${arg4}'"                                                                                                  
#                                                                                                  ${dbgCmd} "+++ arg5 ........: '${arg5}'"                                                                                                  
#        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/deploylib.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${libraryModule} ${arg1} ${arg2}  ${arg3} ${arg4} ${arg5}"
#                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
#        ${l_cmd}
#        RETVAL=$?
#        echo
#        [ $RETVAL -eq 0 ]
#}

statusapp() {
        echo -e $"show status of in current domain (Domain ${domainName}):\n"
                                                                                                  ${dbgCmd} "+++ wlstCmd .....: '${wlstCmd}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ domainName ..: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix : '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ admsrvHost ..: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort ..: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ libraryModule: '${libraryModule}'"
                                                                                                  ${dbgCmd} "+++ arg1 ........: '${arg1}'"
                                                                                                  ${dbgCmd} "+++ arg2 ........: '${arg2}'"
                                                                                                  ${dbgCmd} "+++ arg3 ........: '${arg3}'"
                                                                                                  ${dbgCmd} "+++ arg4 ........: '${arg4}'"
                                                                                                  ${dbgCmd} "+++ arg5 ........: '${arg5}'"
        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/statusapp.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${libraryModule} ${arg1} ${arg2}  ${arg3} ${arg4} ${arg5}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
}


listlibs() {
        echo -e $"show status of in current domain (Domain ${domainName}):\n"
        listlibs="True"
                                                                                                  ${dbgCmd} "+++ wlstCmd ....: '${wlstCmd}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ domainName .: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix: '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ admsrvHost .: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort .: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ listlibs ...: '${listlibs}'"
                                                                                                  ${dbgCmd} "+++ arg1 .......: '${arg1}'"
                                                                                                  ${dbgCmd} "+++ arg2 .......: '${arg2}'"
                                                                                                  ${dbgCmd} "+++ arg3 .......: '${arg3}'"
                                                                                                  ${dbgCmd} "+++ arg4 .......: '${arg4}'"
                                                                                                  ${dbgCmd} "+++ arg5 .......: '${arg5}'"
        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/listapps.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${listlibs} ${arg1} ${arg2}  ${arg3} ${arg4} ${arg5}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
}

listapps() {
        echo -e $"show status of in current domain (Domain ${domainName}):\n"
        listlibs="False"
                                                                                                  ${dbgCmd} "+++ wlstCmd ....: '${wlstCmd}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ domainName .: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix: '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ admsrvHost .: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort .: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ listlibs ...: '${listlibs}'"
                                                                                                  ${dbgCmd} "+++ arg1 .......: '${arg1}'"
                                                                                                  ${dbgCmd} "+++ arg2 .......: '${arg2}'"
                                                                                                  ${dbgCmd} "+++ arg3 .......: '${arg3}'"
                                                                                                  ${dbgCmd} "+++ arg4 .......: '${arg4}'"
                                                                                                  ${dbgCmd} "+++ arg5 .......: '${arg5}'"
        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/listapps.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${listlibs} ${arg1} ${arg2}  ${arg3} ${arg4} ${arg5}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
}

stopapp() {
        echo -e $"stopping deployment in current domain (Domain ${domainName}):\n"
        libraryModule="stop"
                                                                                                  ${dbgCmd} "+++ wlstCmd .....: '${wlstCmd}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ domainName ..: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix : '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ admsrvHost ..: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort ..: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ...: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ arg1 ........: '${arg1}'"
                                                                                                  ${dbgCmd} "+++ libraryModule: '${libraryModule}'"
        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/applicationControl.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${arg1} ${libraryModule}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
}

startapp() {
        echo -e $"starting deployment in current domain (Domain ${domainName}):\n"
        libraryModule="start"
                                                                                                  ${dbgCmd} "+++ wlstCmd ....: '${wlstCmd}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ domainName .: '${domainName}'"
                                                                                                  ${dbgCmd} "+++ domainPrefix: '${domainPrefix}'"
                                                                                                  ${dbgCmd} "+++ admsrvHost .: '${admsrvHost}'"
                                                                                                  ${dbgCmd} "+++ admsrvPort .: '${admsrvPort}'"
                                                                                                  ${dbgCmd} "+++ scriptDir ..: '${scriptDir}'"
                                                                                                  ${dbgCmd} "+++ arg1 .......: '${arg1}'"
                                                                                                  ${dbgCmd} "+++ libraryModule: '${libraryModule}'"
        l_cmd="${wlstCmd} ${scriptDir}/wlsapps/applicationControl.py ${domainName} ${domainPrefix} ${admsrvHost} ${admsrvPort} ${scriptDir} ${arg1} ${libraryModule}"
                                                                                                  ${dbgCmd} "+++ cmd: '${l_cmd}'"
        ${l_cmd}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ]
}
