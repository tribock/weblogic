###
# deploy Library
###
import sys

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]
libraryModule=sys.argv[6]
libName=sys.argv[7]
target=sys.argv[8]



execfile(scriptDir + "/connectionUtil.py")
#get user home
from os.path import expanduser
home = expanduser("~")

old_stdout = sys.stdout
sys.stdout = "something"
connectToAdminServer(home, domName, admsrvHost, admsrvPort)


#========================
#Application deployment Section
#========================


try:
    domainRuntime()
    cd('domainRuntime:/AppRuntimeStateRuntime/AppRuntimeStateRuntime')
    currentState = cmo.getCurrentState(libName, target)
    sys.stdout = old_stdout
    if currentState == "STATE_ACTIVE":
        print
        '%52s %1s %25s %10s' % (libName, ':', '\033[1;32m' + currentState + '\033[0m', '(' + target + ')')
    else:
        print
        '%52s %1s %25s %10s' % (libName, ':', '\033[1;31m' + currentState + '\033[0m', '(' + target + ')')
except:
    print 'Error in getting current status of ' +libName+ ' on  ' +target+ '\n'
    exit(exitcode=1)
