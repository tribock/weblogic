import sys
import os
from java.io import File
from java.io import FileOutputStream

# ------------------------------------------------------------
# --  Reading script arguments  ------------------------------
# ------------------------------------------------------------
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
applicationAction=sys.argv[5]
deploymentName=sys.argv[6]
deploymentFile=sys.argv[7]
deploymentTarget=sys.argv[8]
# ------------------------------------------------------------
# --  Reading environment variables  -------------------------
# ------------------------------------------------------------
userHome=os.environ['HOME']

# ------------------------------------------------------------
# --  Connect to Admin Server  -------------------------------
# ------------------------------------------------------------
execfile(scriptDir + "/connectionUtil.py")
#get user home
from os.path import expanduser
home = expanduser("~")

connectToAdminServer(home, domName, admsrvHost, admsrvPort)

#========================
#Checking Application Status Section
#========================

def appstatus(deploymentName, deploymentTarget):

  try:
    cd('domainRuntime:/AppRuntimeStateRuntime/AppRuntimeStateRuntime')
    currentState = cmo.getCurrentState(deploymentName, deploymentTarget)
    return currentState
  except:
    print 'Error in getting current status of ' +deploymentName+ '\n'
    exit()

#========================
#Application undeployment Section
#========================

def undeployApplication():

  try:
    print 'stopping and undeploying ..' +deploymentName+ '\n'
    stopApplication(deploymentName, targets=deploymentTarget)
    undeploy(deploymentName, targets=deploymentTarget)
  except:
    print 'Error during the stop and undeployment of ' +deploymentName+ '\n'

#========================
#Applications deployment Section
#========================

def deployApplication():

  try:
    print 'Deploying the application ' +deploymentName+ '\n'
    deploy(deploymentName,deploymentFile,targets=deploymentTarget)
    startApplication(deploymentName)
  except:
    print 'Error during the deployment of ' +deploymentName+ '\n'
    exit()

#========================
#Main Control Block For Operations
#========================

def deployUndeployMain():

  try:
    if appstatus(deploymentName, deploymentTarget)=='STATE_RETIRED' :
      appflag=1
    elif appstatus(deploymentName, deploymentTarget)=='STATE_ACTIVE':
      appflag=2
    else:
      print ' other Applications are Running '
      pass
    
    if appflag==1 :
      print 'application having RETIERED STATE ..'
      undeployApplication()
      print appstatus(deploymentName, deploymentTarget)
      deployApplication()
      print appstatus(deploymentName, deploymentTarget)
    elif appflag==2:
      print 'Application exists in ACTIVE state...'
      undeployApplication()
      deployApplication()
      print appstatus(deploymentName, deploymentTarget)
    else:
      print 'new application'
      deployApplication()
  except:
    print 'Error during the deployment of ' +deploymentName+ '\n'
    exit()

#========================
#Execute Block
#========================

if applicationAction=='statusapp' :
  connectToDomain()
  print appstatus(deploymentName, deploymentTarget)
  disconnect()
  exit()
  
if applicationAction=='deployapp' :
  connectToDomain()
  deployUndeployMain()
  disconnect()
  exit()