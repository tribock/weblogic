import os

domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]

execfile(scriptDir + "/managedServerUtil.py")
execfile(scriptDir + "/connectionUtil.py")

#get user home
from os.path import expanduser
home=expanduser("~")

#connect to Adminserver
connectToAdminServer(home, domName, admsrvHost, admsrvPort)

# ------------------------------------------------------------
# --  Set the Upload - Directory  ----------------------------
# ------------------------------------------------------------

edit()
startEdit()

cd('/Servers/' + domPrefix + '_AdminServer')
cmo.setStagingMode('nostage')
cmo.setUploadDirectoryName( '/u01/app/oracle/deployments/' + domName)

activate()

disconnect()

exit()

