import sys
import os
import string
#   Reading script arguments and echo the values  
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]

maxFileArg="" + sys.argv[6] + ""

execfile(scriptDir + "/managedServerUtil.py")
execfile(scriptDir + "/connectionUtil.py")
execfile(scriptDir + "/wlsconfig/logParam/logfileUtil.py")
#get user home
from os.path import expanduser
home=expanduser("~")

#connect to Adminserver
connectToAdminServer(home, domName, admsrvHost, admsrvPort)

#   Initialise basic Variables -
allSrvs=[]
maxFile=maxFileArg

domainConfig()
serverList=cmo.getServers();
edit()
startEdit()

#   Set the values for the domain  -

path='/Log/' + domName
FName='/u01/app/oracle/logs/' + domName + '/_domain/' + domName + '.log'
SetValues(path,maxFile,FName )
print("Einstellungen fuer die domain '" + domName + "' gemacht")

for srv in serverList:
  setServerLogValues(srv.getName(),maxFile)

print("settings for the JMS Servers will be applied")

# step into JMSServer directory
cd('/JMSServers')

# create Array which contains all Servers listed in current directory/object
JMSServers = cmo.getJMSServers()

# iterate through array of JMSServer

for JMSsrv in JMSServers:
  JMSsrvName = JMSsrv.getName()
  path='/JMSServers/' + JMSsrvName + '/JMSMessageLogFile/' + JMSsrvName

  FName='/u01/app/oracle/logs/' + domName + '/' + JMSsrvName + '/' + JMSsrvName + '.log'
  SetValues(path,maxFile,FName )
  print("- JMSServer-Log for " + JMSsrvName +" ist angepasst")

save()
activate()
disconnect()
exit()