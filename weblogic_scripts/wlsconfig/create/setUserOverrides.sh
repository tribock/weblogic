#!/bin/sh

# Determine Domainname with cutting the DOMAIN_HOME var
export DOMAIN_NAME=${DOMAIN_HOME##*/}

# Define Cluster Name out of Server Name
CLUSTER_NAME=${SERVER_NAME/Server/Cluster}
export CLUSTER_NAME=${CLUSTER_NAME%?}

prefix="${SERVER_NAME:0:3}"
odwekSuffix="${prefix,,}"
case "${prefix:0:1}" in
#  W )
#    odwekSuffix="0"
#  ;;
  E )
    odwekSuffix="1"
  ;;
  P )
    odwekSuffix="2"
  ;;
  * )
  ;;
esac  

LOG_TYPE="setUserOverrides"

# Disable Schema Validation
JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.configuration.schemaValidationEnabled=false"

# Delete Temp and Cache of server (only if server $SERVER_NAME is not running)
# 20170703-PY359: Abfrage nach AdminServer auskomentiert --> cache und tmp werden beim starten bei allen Servern gelöscht
#if [[ ${SERVER_NAME} == *"_AdminServer" ]];
#then
#   echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <${SERVER_NAME} - cache and tmp will not be deleted>"
#else
   if [ -d /opt/exalogic.tools ] ; then
      export amountRunningProc=`/u01/common/general/scripts/all_cn/allcn_do.sh "ps -ef | grep ${SERVER_NAME} | grep -v grep" | grep -v 'Target nodes:' | wc -l`
   else
      export amountRunningProc=`ps -ef | grep ${SERVER_NAME} | grep -v grep | wc -l`
   fi

   if [ ${amountRunningProc} -eq 0 ]
   then
      echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <No active ${SERVER_NAME} processes found - cache and tmp will be deleted>"
      rm -rf ${DOMAIN_HOME}/servers/${SERVER_NAME}/cache
      rm -rf ${DOMAIN_HOME}/servers/${SERVER_NAME}/tmp
   else
      echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Active ${SERVER_NAME} processes found - cache and tmp will not be deleted>"
   fi
#fi


# Add Libraries to Servers Classpath (Logger)
export CLASSPATH=${CLASSPATH}":${DOMAIN_HOME}/lib/*"

# Enable CDI
JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.jersey.server.impl.cdi.lookupExtensionInBeanManager=true"

# No Hostname Verification at all
JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.security.SSL.ignoreHostnameVerification=true"

# Disallow ReverseDNS
JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.ReverseDNSAllowed=false"

# Only TLS V1.0 messages are sent and accepted ie. SSL 2.0/3.0 disabled
JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.security.SSL.protocolVersion=TLS1"

# Prepare for new Garbage Collection
JAVA_OPTIONS="${JAVA_OPTIONS} -XX:+UseG1GC"

# For better performance (SR 3-13935580311)
# JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.UseEnhancedIncrementAdvisor=false "

# Set Logfilerights for *.out and *.err according to umask
JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.DefaultLogFilePermissionsEnabled=false"

# Configure user.timezone (Workaround for EK Loganto calculated dates)
# Europe/Zurich - no daylight saving time 07.04.1980 - 30.09.1980
JAVA_OPTIONS="${JAVA_OPTIONS} -Duser.timezone=Europe/Zurich"


# Set some_companyname_HOME Variable and add some memory
if [[ "${SERVER_NAME}" == *"_AdminServer" ]] ; then

        # Workaround weblogic.socket.MaxMessageSizeExceededException
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.management.disableManagedServerNotifications=true"

        # Define for each domain it's own wlst tempdir
        WLST_PROPERTIES="${WLST_PROPERTIES} -Djava.io.tmpdir=/u01/app/oracle/domains/${DOMAIN_NAME}/wlsttmp"
        
        CLASSES_DIR=/u01/app/oracle/resources/${DOMAIN_NAME}/${SERVER_NAME}/classes
        LIB_DIR=/u01/app/oracle/resources/${DOMAIN_NAME}/${SERVER_NAME}/lib

else
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.domainName=${DOMAIN_NAME}"
        
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.clusterName=${CLUSTER_NAME}"
        
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.serverName=${SERVER_NAME}"
        
        export some_companyname_HOME="/u01/app/oracle/resources/${DOMAIN_NAME}/${CLUSTER_NAME}"
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Exporting some_companyname_HOME to $some_companyname_HOME>"
        
        CLASSES_DIR=${some_companyname_HOME}/classes
        LIB_DIR=${some_companyname_HOME}/lib

        # Set some_companyname_TRANSFER Variable
        export some_companyname_TRANSFER="/u01/app/oracle/transfer/${DOMAIN_NAME}/${CLUSTER_NAME}"
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Exporting some_companyname_TRANSFER to $some_companyname_TRANSFER>"
                                                                                                
        #(Doc ID 2125331.1)
        JAVA_OPTIONS="${JAVA_OPTIONS} -DANTLR_USE_DIRECT_CLASS_LOADING=true"
                
        # Number of method invocations/branches before compiling
        JAVA_OPTIONS="${JAVA_OPTIONS} -XX:CompileThreshold=8000"
        
        # Generate memory heap dump on OutOfMemory Exception
        JAVA_OPTIONS="${JAVA_OPTIONS} -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/u01/common/general/dumps/${SERVER_NAME}_$$.hdump"
        JAVA_OPTIONS="${JAVA_OPTIONS} -XX:OnOutOfMemoryError=${DOMAIN_HOME}/bin/killWebLogic.sh"

fi

# Add Class Files to Server Classpath
if [ -d "${CLASSES_DIR}" ] ; then
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Adding ${CLASSES_DIR} to CLASSPATH:>"
        export CLASSPATH=${CLASSPATH}:${CLASSES_DIR}
fi

# Add Libraries to Server Classpath
if [ -d "${LIB_DIR}" ] ; then
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Adding ${LIB_DIR} to CLASSPATH:>"
        export CLASSPATH=${CLASSPATH}:${LIB_DIR}/*
fi
echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Configured Classpath for ${SERVER_NAME}: ${CLASSPATH}>"

# Faster startup (Do not verify the bytecode Note: This parameter can reduce startup time, but you might lose some of the protection provided by Java)
JAVA_OPTIONS="${JAVA_OPTIONS} -Xverify:none"

# Enable stack trace print in production
JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.PrintStackTraceInProduction=true"

# special settings for ExaLogic systems
if [[ "${HOSTNAME}" == "el"* ]]; then
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Activating special options for ExaLogic>"
# Enable SDP Support for JDBC Connections on ExaLogic Systems
        JAVA_OPTIONS="${JAVA_OPTIONS} -Djava.net.preferIPv4Stack=true -Doracle.net.SDP=true"

# Prepare for Flight Recorder activation on ExaLogic Systems
        JAVA_OPTIONS="${JAVA_OPTIONS} -XX:+UnlockCommercialFeatures -XX:+ResourceManagement"

# For better performance on ExaLogic Systems (SR 3-13935580311)
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.UseEnhancedPriorityQueueForRequestManager=true"
fi


# Set Application dedicated JAVA_OPTIONS (all domains)

# eDesk Clusters
if [[ "${SERVER_NAME}" == *"_eDeskServer"* ]] ; then
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for eDeskServer...>"
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.wsee.workarea.skipWorkAreaHeader=true"

        # Force JVM to use custom keystore as described in https://docs.oracle.com/cd/E19159-01/820-1072/6ncp48v4d/index.html
        JAVA_OPTIONS="${JAVA_OPTIONS} -Djavax.net.ssl.trustStore=${DOMAIN_HOME}/security/exalogictrust.jks"
fi

# Set Options for VI Clusters (all)
if [[ "${SERVER_NAME}" == *"_VI"* ]]; then
      	echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for VIServer...>"
        JAVA_OPTIONS="${JAVA_OPTIONS} -Doracle.jdbc.DateZeroTime=true"
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dcom.sun.xml.ws.spi.db.BindingContextFactory=com.sun.xml.ws.db.glassfish.JAXBRIContextFactory" 
        JAVA_OPTIONS="${JAVA_OPTIONS} -Djavax.xml.bind.JAXBContext=com.sun.xml.bind.v2.ContextFactory" 
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dorg.apache.cxf.Logger=org.apache.cxf.common.logging.Log4jLogger" 
        JAVA_OPTIONS="${JAVA_OPTIONS} -Djava.awt.headless=true"
  # Activate ODWEK
        export ARS_INSTALL_ODWEK_V90_DIR="/u01/app/oracle/product/9.0.0/odwek_${odwekSuffix}"
	echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Adding ODWEK Path ${ARS_INSTALL_ODWEK_V90_DIR} to LD_LIBRARY_PATH...>"
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ARS_INSTALL_ODWEK_V90_DIR

fi

# Set Options for BaseServiceServer
if [[ "${SERVER_NAME}" == *"_BaseServiceServer"* ]]; then
      	echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for BaseServiceServer...>"
  if [[ "${SERVER_NAME}" == "T"* || "${SERVER_NAME}" == "P"* ]]; then
  # Activate ODWEK
        export ARS_INSTALL_ODWEK_V90_DIR="/u01/app/oracle/product/9.0.0/odwek_${odwekSuffix}"
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Adding ODWEK Path ${ARS_INSTALL_ODWEK_V90_DIR} to LD_LIBRARY_PATH...>"
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ARS_INSTALL_ODWEK_V90_DIR
  fi
fi

# Set Options for eTaxesServer
if [[ "${SERVER_NAME}" == *"_eTaxesServer"* ]]; then
      	echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for eTaxesServer...>"
         JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.wsee.workarea.skipWorkAreaHeader=true"

         # Force JVM to use custom keystore as described in https://docs.oracle.com/cd/E19159-01/820-1072/6ncp48v4d/index.html
         JAVA_OPTIONS="${JAVA_OPTIONS} -Djavax.net.ssl.trustStore=${DOMAIN_HOME}/security/exalogictrust.jks"
fi

# Set Options for ZPServiceServer
if [[ "${SERVER_NAME}" == *"_ZPServiceServer"* ]]; then
      	echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for ZPServiceServer...>"
         JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.wsee.workarea.skipWorkAreaHeader=true"
fi

# Set Options for WebServiceServer
if [[ "${SERVER_NAME}" == *"_WebServiceServer"* ]]; then
      	echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for WebServiceServer...>"
         JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.wsee.workarea.skipWorkAreaHeader=true"
fi

# Set Options for eGovBoxServer
if [[ "${SERVER_NAME}" == *"_eGovBoxServer"* ]] ; then
        echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Setting standard JAVA-Options for eGovBoxServer...>"
        JAVA_OPTIONS="${JAVA_OPTIONS} -Dweblogic.wsee.workarea.skipWorkAreaHeader=true"

        # The following setting is necessary for the Jasper Reports in ePandemie to function correctly. Jasper will be removed once iMessage becomes active and then this entry will not be needed anymore
        JAVA_OPTIONS="${JAVA_OPTIONS} -Djava.awt.headless=true"
fi

# add individual otions from serverValues.csv
scriptBase="/u01/common/general/scripts/"
if [[ "${SERVER_NAME}" == "W"* ]]; then
         scriptBase="/u01/common/general/testcommon/scripts/"
fi

echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Activating individual options for Server ${SERVER_NAME}>"
JAVA_OPTIONS="${JAVA_OPTIONS} `${scriptBase}values/serverValues.sh JavaOptionen ${SERVER_NAME}`"


if [[ "${SERVER_NAME}" != "P"* && "${SERVER_NAME}" != "S"* && "${SERVER_NAME}" != *"_AdminServer" ]]; then

        PORT1=`${scriptBase}values/serverValues.sh value ${SERVER_NAME} ApplPort only`
        if [[ ! "${PORT1}" == "ERROR"* ]]
        then
            DEBUG_PORT=`expr $PORT1 + 10000`
            JPROFILER_PORT=`expr $PORT1 + 10001`     


            echo "<`date +"%h %d, %Y %l:%M:%S %p %Z" | sed 's+  + +g'`> <INFO> <${LOG_TYPE}> <Activating Remote Debugging / JProfiler for Server ${SERVER_NAME} on Port ${DEBUG_PORT} / ${JPROFILER_PORT}>"

#           Activate Remote Debugging
            JAVA_OPTIONS="${JAVA_OPTIONS} -Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,address=${DEBUG_PORT},server=y,suspend=n -Djava.compiler=NONE"

#           Activate JProfiler
            jProfilerVers=`${scriptBase}values/domainValues.sh value ${prefix} activJProfilerVersion only`
        
            if [[ "${jProfilerVers}" != "-" && "${jProfilerVers}" != "ERROR"* ]]; then
                  JAVA_OPTIONS="${JAVA_OPTIONS} -agentpath:/u01/app/oracle/product/${jProfilerVers}/jprofiler_${prefix,,}/bin/linux-x64/libjprofilerti.so=port=${JPROFILER_PORT},nowait"
            fi
        fi

fi


export MEM_ARGS="`${scriptBase}values/serverValues.sh value ${SERVER_NAME} memory only`"
export JAVA_OPTIONS
export WLST_PROPERTIES
