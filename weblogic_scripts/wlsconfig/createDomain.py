# inspired by http://blog.darwin-it.nl/2016/06/scripted-domain-creation-for-soabpm-osb.html 
# Modify these values as necessary

import sys, traceback
import socket;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import os.path;
import os.system;

domPrefix = sys.argv[1]
ndmgrPort = sys.argv[2]
wlsVersion = sys.argv[3]
javaVersion = sys.argv[4]

scriptName = createDomain

#
# ------------------------------------------------------------------------------
# defines local functions

#
# Determine the Server Java Args
def getServerJavaArgs(serverName,logsHome):
  javaArgs = '-Dweblogic.Stdout = ' + logsHome + '/' + serverName + '/' + serverName + '.out -Dweblogic.Stderr = ' + logsHome + '/' + serverName + '/' + serverName + '.err'
  return javaArgs

#
# Create Unix Machine(s)
def createUnixMachines(domainHome, numMachines, nmHost, nmPort):
  print ('\nCreate Unix machines: numMachines=' +str(numMachines))
  nmHome = '/u01/app/oracle/domains/' + domainName + '/nodemanager'
  iMachine = 0
  
  while iMachine < numMachines:
    iMachine = iMachine + 1
    serverMachine = machineName + str(iMachine)
    print ('. Create machine ' + serverMachine + ' with type UnixMachine')
    serverAddress = nmHost
    if numMachines > 1:
      print ('. Create machine for Exa')
      serverAddress = (serverAddress[:3]) + str(iMachine) + '-priv'
      nmHome = domainHome + '/' + serverAddress + '/nodemanager'
    cd ('/')

    create (serverMachine,'UnixMachine')
    cd ('UnixMachine/' + serverMachine)
    create (serverMachine,'NodeManager')
    cd ('NodeManager/' + serverMachine)
    set ('ListenAddress',serverAddress)
    set ('ListenPort',int(nmPort))
    set ('NMType',nmType)

    if numMachines > 1:
      set ('NodeManagerHome',nmHome)

  print ('. all Unix machines are created now')

def defineAccessLog(serverName):
  # access log
  cd('/Server/'+serverName)
  print 'change access logname'
  create(serverName,'WebServer')
  cd('WebServer/'+serverName)
  create(serverName,'WebServerLog')
  cd('WebServerLog/'+serverName)
  set('LogFileFormat' , 'extended')
  set('FileCount' , 62)
  set('ELFFields', 'date time c-ip c-dns s-ip s-dns cs-uri cs-uri-stem cs-uri-query ctx-ecid ctx-rid cs-method sc-status sc-comment time-taken bytes')
  set('FileName', logsHome+'/'+serverName+'.access')

    
#
# Change Admin Server
def changeAdminServer(adminServerName, listenAddress, listenPort, javaArguments):
  print ('\nChange AdminServer')
  
  cd ('/Servers/AdminServer')
  
  # name of adminserver
  print '. Set Name to ' + adminServerName
  set ('Name',adminServerName )
  cd ('/Servers/' + adminServerName)
  
  # Machine
  serverMachine = machineName + '1'
  print '. Set Machine to ' + serverMachine
  set ('Machine',serverMachine)
  
  # address and port
  print '. Set ListenAddress to ' + adminListenAddress
  set ('ListenAddress',adminListenAddress)
  print '. Set ListenPort to ' + str(listenPort)
  set ('ListenPort',int(listenPort))
  set ('MaxMessageSize',40000000)

  # ServerStart
  print ('. Create ServerStart')
  create (adminServerName,'ServerStart')
  cd ('ServerStart/' + adminServerName)
  print '. Set Arguments to: ' + javaArguments
  set ('Arguments' , javaArguments)

  # access log
  defineAccessLog(adminServerName)    

  # SSL
  print '\nSet SSL'
  cd ('/Servers/' + adminServerName )
  create (adminServerName,'SSL')
  cd ('SSL/' + adminServerName)
  set ('Enabled'                    , 'False')
  set ('HostNameVerificationIgnored', 'True')
  print '. SSL defined, but not jet activated!' 
  
#
# ------------------------------------------------------------------------------
#

#
# Set Properties ...
print 'Set Properties'

fileName = '/tmp/' + domPrefix + '_Config.dat'
print ('. File-Name: ' + fileName )

scriptDir = os.path.dirname(sys.argv[0])

execfile(scriptDir + "/readConfigFile.py")
print ('. successfully insourcing of "' + scriptDir + '/readConfigFile.py"' )

#
# Read and prozess 'DOM'-Record from file + fileName
readConfigFile('DOM')
domLines = lines.split(';')
print ('. Anz. DOM-Lines: ' + str(len(domLines)))
items = domLines[0].split(' | ')
(domShortName, typ, domainName, adminUser, adminPw, adminPort, ndmgrUser, ndmgrPw, identityStoreFile, identityStorePw, trustStoreFile, trustStorePw, logFilter, Providerlist, Filler) = items
print ('. DOM-Record: ' + domShortName + ', ' + typ + ', ' + domainName + ', ' + adminUser + ', ' + adminPw + ', ' + adminPort + ', ' + ndmgrUser + ', ' + ndmgrPw + ', ' + identityStoreFile + ', ' + identityStorePw + ', ' + trustStoreFile + ', ' + trustStorePw + ', ' + logFilter + ', ' + Providerlist)

host = socket.gethostname()
java_home = '/u01/app/oracle/product/' + javaVersion + '/jdk_' + domPrefix
wlHome = '/u01/app/oracle/product/' + wlsVersion + '/mwhome_' + domPrefix
templateHome = wlHome + '/wlserver/common/templates/wls'
domainTemplate = templateHome + '/wls.jar'
domainsHome = '/u01/app/oracle/domains'
domainHome = domainsHome + '/' + domainName
wlsHome = wlHome + '/wlserver'

providers = Providerlist.split('|')
for iProv in range(0,len(providers)):
  if providers[iProv] == 'etaxesAuthenticationProvider':
    print ('. provider ' + providers[iProv] + ' is needed' )
    print ('. cp /u01/common/general/scripts/create/EtaxesTaxPayerAuthenticationProvider.jar ' + wlsHome + '/server/lib/mbeantypes/.' )
    os.system ('cp /u01/common/general/scripts/create/EtaxesTaxPayerAuthenticationProvider.jar ' + wlsHome + '/server/lib/mbeantypes/.')
    
print ('. Domain environment is set now' )

#
# Read and prozess 'ADM'-Record from file + fileName
readConfigFile('ADM')
admLines = lines.split(';')
print ('. Anz. ADM-Lines: ' + str(len(admLines)))
items = admLines[0].split(' | ')
(domShortName, typ, Fill1, adminServerName, adminListenPort, admSSL, admSSLPort, adminListenAddress, Filler) = items
doExit = 0
if domShortName == '':
  domShortName = '[Value missing]'
  doExit = 1
if typ == '':
  typ = '[Value missing]'
  doExit = 1
if adminServerName == '':
  adminServerName = '[Value missing]'
  doExit = 1
if adminListenPort == '':
  adminListenPort = '[Value missing]'
  doExit = 1
if admSSL == '':
  admSSL = '[Value missing]'
  doExit = 1
if admSSLPort == '':
  admSSLPort = '[Value missing]'
  doExit = 1
if adminListenAddress == '':
  adminListenAddress = '[Value missing]'
  doExit = 1
  
print ('. ADM-Record: ' + domShortName + ', ' + typ + ', ' + adminServerName + ', ' + adminListenPort + ', ' + admSSL + ', ' + admSSLPort + ', ' + adminListenAddress)

if doExit == 1:
  print ('--> Action cancelled')
  exit (1)
  
machineName = domPrefix.upper() + '_Machine0'
logsHome = '/u01/app/oracle/logs/' + domainName
iMachine = 0
if (host[:2]) == 'el':        
  Environment = 'exa'
  nmHost = (host[4:8]) + '-priv'
  numMachines = 4 
  oracleHome = '/u01/home/oracle' 
else:
  Environment = 'non-exa'
  nmHost = host
  numMachines = 1                 
  oracleHome = '/home/oracle' 
nmPort = ndmgrPort
nmType = 'plain'
print ('. Admin environment is set now' )

try:
    #
    # Section 1: Base Domain + Admin Server
    print ('\nCreate Base domain ' + domainName)

    print ('. Create base wls domain with template ' + domainTemplate)
    createDomain(domainTemplate,domainHome,adminUser,adminPw)
    readDomain(domainHome)

    # Domain Log
    print ('\nSet domain log')
    cd ('/')
    create (domainName,'Log')
    cd ('/Log/' + domainName)
    set ('FileName', '/u01/app/oracle/logs/' + domainName + '/' + domPrefix.upper() + '_domain/' + domainName + '.log')
    set ('RotationType','byTime')
    set ('NumberOfFilesLimited', true)
    set ('FileCount', 62)
    set ('FileTimeSpan', 24)
    set ('RotationTime', '00:00')
    set ('RotateLogOnStartup', true)
    set ('LogFileRotationDir', '')
    print ('. domain log is set now')

    # Machine(s)    
    createUnixMachines(domainHome, numMachines, nmHost, nmPort)

    # Admin Server
    adminJavaArgs = getServerJavaArgs(adminServerName,logsHome)
    # print ('\n+++ call function: changeAdminServer(' + adminServerName + ',' + adminListenAddress + ',' + str(adminListenPort) + ',' + adminJavaArgs + ')')
    changeAdminServer(adminServerName,adminListenAddress,adminListenPort,adminJavaArgs)  
    
    # set StartMode
    print ('\nSet ServerStartMode to prod')
    cd ('/')
    set ('ProductionModeEnabled','true')
    print ('. ServerStartMode is set now')

    # set NDMGR-Home
    print ('\nSet NDMGR-Home')     
    nmHome = '/u01/app/oracle/domains/' + domainName + '/nodemanager'
    cd ('/')
    cd ('NMProperties')
    set ('NodeManagerHome', nmHome)
    set ('SecureListener', 'false')
    set ('ListenAddress', nmHost)
    set ('ListenPort', nmPort)
    print ('. NDMGR-Home is set now')

    # set nodemanager-pwd
    print ('\nSet nodemanager password')
    cd ('/SecurityConfiguration/' + domainName)
    print ('. ndmgrUser: ' + ndmgrUser)
    print ('. ndmgrPw: ' + ndmgrPw)
    print ('. domainHome: ' + domainHome)
    esn = encrypt(ndmgrPw,domainHome)
    # print ('. esn: ' + esn)
    set ('NodeManagerUsername', ndmgrUser )
    set ('NodeManagerPasswordEncrypted', esn )
    print ('. Nodemanager user and password is set')

    # set JTA-Parms
    print ('\nSet JTA parms')
    cd ('/')
    create (domainName,'JTA')
    cd ('JTA/' + domainName)
    set ('CrossSiteRecoveryLeaseExpiration', 604800)
    set ('CrossSiteRecoveryLeaseUpdate', 604800)
    set ('CrossSiteRecoveryRetryInterval', 604800)
    print ('. JTA is set now')

    # set Console Cookiename
    print ('\nSet Console Cookiename')
    cd ('/')
    create (domainName,'AdminConsole')
    cd ('/AdminConsole/' + domainName)
    set ('CookieName', 'ADMINCONSOLESESSION_' + domPrefix)   
    print ('. Console Cookiename is set now')

    # set Exalogic Optimization    
    if Environment == 'exa':
      print ('\nSet Exalogic Optimization')
      cd ('/')
      set ('ExalogicOptimizationsEnabled', 'true')   
      print ('. Exalogic Optimization is set now')

    updateDomain()
    closeDomain();
    print ('\n"updateDomain" and "closeDomain" done\n')


    print ('\ncreate Nodemanager-Directory')
    for iMachine in range (1,numMachines + 1):
      print ('. for hostsystem "' + host + '"')
      nmHomeDir = domainHome + '/nodemanager'
      nmPropertyFile = nmHomeDir + '/nodemanager.properties'
      if numMachines > 1:
        nmHost = (host[4:7]) + str(iMachine)
        nmListenAddress = nmHost + '-priv'
        nmHomeDirNew = domainHome + '/' + nmHost + '/nodemanager'
        print ('. copy File ' + nmHomeDir + '/nodemanager.domains to ' + nmHomeDirNew + '/nodemanager.domains')
        os.system ('mkdir -p ' + nmHomeDirNew)
        os.system ('cp ' + nmHomeDir + '/nodemanager.domains ' + nmHomeDirNew + '/nodemanager.domains')
        nmLogFileName = logsHome + '/' + domPrefix.upper() + '_Nodemanager/' + domPrefix.upper() + '_Nodemanager_' + nmHost + '.log' 
      
      else:
        nmHomeDirNew = nmHomeDir  
        print ('. copy File ' + nmHomeDir + '/nodemanager.properties to ' + nmHomeDirNew + '/nodemanager_ori.properties')
        os.system ('cp ' + nmHomeDir + '/nodemanager.properties ' + nmHomeDirNew + '/nodemanager_ori.properties')
        nmLogFileName = logsHome + '/' + domPrefix.upper() + '_Nodemanager/' + domPrefix.upper() + '_Nodemanager.log' 

      print ('. Read Nodemanager properties file: ' + nmPropertyFile)
      f = open (nmPropertyFile)
      nmProps = ''
      for line in f.readlines():
        if numMachines > 1:
          if line.strip().startswith ('DomainsFile'):
            line = 'DomainsFile=' + nmHomeDirNew + '/nodemanager.domains\n'
          if line.strip().startswith ('NodeManagerHome'):
            line = 'NodeManagerHome=' + nmHomeDirNew + '\n'
          if line.strip().startswith ('ListenAddress'):
            line = 'ListenAddress=' + nmListenAddress + '\n'
        # making sure these properties are set to true:
        if line.strip().startswith ('SecureListener'):
          line = 'SecureListener=false\n'
        if line.strip().startswith ('QuitEnabled'):
          line = 'QuitEnabled=true\n'
        if line.strip().startswith ('LogFile'):
          line = 'LogFile=' + nmLogFileName + '\n'
        nmProps = nmProps + line
      if numMachines > 1:        
        nmProps = nmProps + 'DomainsDirRemoteSharingEnabled = true\n'

      # Create new file
      print ('\nnew property file for host ' + nmHost)
      print ('===========================')
      print nmProps 
      print ('===========================')

      # Save New File
      nmPropertyFile = nmHomeDirNew + '/nodemanager.properties'
      print ('. properties saved to ' + nmPropertyFile)
      fileNew = open  (nmPropertyFile, 'w')
      fileNew.write (nmProps)
      fileNew.flush ()
      fileNew.close ()

    print ('\nFinished creating Domain ' + domainName + ' (Offline)')
    print ('\nNext steps to finalize the domain:')
    print ('1. Start the nodemanager (use the command below)')
    print ('2. Start the adminserver (use the command below)')
    print ('3. If nodemaneger and adminserver ist running ...')
    print ('   Start Part2 manually with Command: "' + scriptDir + '/wlsconfig.sh ' + domPrefix + ' updateDomain ' + fileName + '"' )
    print ('\nExiting...')
    exit()
   
except NameError, e:
    print 'Apparently properties not set.'
    print "Please check the property: ", sys.exc_info()[0], sys.exc_info()[1]
except:
    apply (traceback.print_exception, sys.exc_info())
    stopEdit ('y')
    exit (exitcode = 1)

exit ()