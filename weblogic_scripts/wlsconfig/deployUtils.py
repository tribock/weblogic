def deploy_Libraries():
  edit()
  startEdit()
  readConfigFile('LIB')
  if lines != '':
    print ('. start deploy_Libraries()')
    libLines = lines.split(';')
    print ('Anz. LIB-Lines: ' +str(len(libLines)))

    try:
      for iLines in range(0,len(libLines)):
        items = libLines[iLines].split(' | ')
        # print ('LIBLine: ' +str(iLines)+ '  ' +libLines[iLines])
        # print ('Anz items Library: ' + str(len(items)))
        (domShortName, typ, libraryTargets, libraryName, libraryFile, Filler) = items
        # print(str(iLines)+ ' Library:   '+typ+ ', '+libraryTargets+ ', '+libraryName+ ', '+libraryFile)
        # ref = getMBean('/Libraries/'+libraryName)
        # if ref==None:
        libNames = libraryName.split('#')
        if len(libNames) > 1: 
          libVersions = libNames[1].split('@')
          deploy(appName=libNames[0], path=libraryFile, targets=libraryTargets, libraryModule='true', libImplVersion=libVersions[1], libSpecVersion=libVersions[0])
        else:
          deploy(appName=libNames[0], path=libraryFile, targets=libraryTargets, libraryModule='true')
        print ('. Library ' + libraryName + ' deployed and tagreted on ' + libraryTargets)    
    except Exception, e:
      print('exception ' +e)
      pass
  activate()

# 

def deploy_Applications():
  edit()
  startEdit()
  readConfigFile('APP')
  if lines != '':
    print ('. start deploy_Applications()')
    appLines = lines.split(';')
    print ('Anz. APP-Lines: ' +str(len(appLines)))

    try:
      for iLines in range(0,len(appLines)):
        items = appLines[iLines].split(' | ')
        # print ('APPLine: ' +str(iLines)+ '  ' +appLines[iLines])
        # print ('Anz items Application: ' + str(len(items)))
        (domShortName, typ, applicationTargets, applicationName, applicationFile, planFile, deploymentPriority, Filler) = items
        # print(str(iLines)+ ' Applications:   '+typ+ ', '+applicationTargets+ ', '+applicationName+ ', '+applicationFile+ ', '+planFile+ ', '+deploymentPriority)

        applNames = applicationName.split('#')
        redirect ('/dev/null', 'false')
        stopApplication(applNames[0])
        undeploy(applNames[0])
        redirect ('/dev/null', 'true')

        if deploymentPriority == '':
          deploymentPriority = 100        
      
        try:
          redirect ('/dev/null', 'false')
          deploy(appName=applNames[0], path=applicationFile, targets=applicationTargets, deploymentOrder=deploymentPriority)
          redirect ('/dev/null', 'true')
          print ('. Application ' + applNames[0] + ' deployed and tagreted on ' + applicationTargets )    
        except Exception, e:
          redirect ('/dev/null', 'true')
          print ('. ERROR while deployment of ' + applNames[0] + ' on ' + applicationTargets + ', exeption:' + str(e))    
            
        if planFile != '':
          try:
            print('### update ' +applNames[0]+ ' with plan: ' +planFile) 
            redirect ('/dev/null', 'false')
            updateApplication(appName=applNames[0], planPath=planFile)
            redirect ('/dev/null', 'true')
            print ('. . Deployment updated with plan ' + planFile )    
          except :
            redirect ('/dev/null', 'true')
            print ('. ERROR while applying the plan ' + planFile )    
          
        startApplication(applNames[0])

    except Exception, e:
      redirect ('/dev/null', 'true')
      print('exception ' + str(e))
      pass

  activate()

#