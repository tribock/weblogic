# import sys
import os.path

domPrefix = sys.argv[1]
domainName = sys.argv[2]
adminListenAddress = sys.argv[3]
adminListenPort = sys.argv[4]
Environment = sys.argv[5]
scriptName = 'deployApp'

#
# ------------------------------------------------------------------------------
# defines local functions

def deploy_Libs_Apps(Environment):
  #
  # Connect to AdminServer 
  print ('\nConnect to AdminServer')
  home='/home/oracle'
  if Environment=='exa':
    home='/u01/home/oracle'  
  print ('+++ connectToAdminServer(' + home + ', ' + domainName + ', ' + adminListenAddress + ', ' + adminListenPort + ')')
  connectToAdminServer(home, domainName, adminListenAddress, adminListenPort)
  print ('. Connectet to AdminServer')
  
  #
  # Deploy Libraries
  # print ('\nDeploy Libraries')
  deploy_Libraries()
  # print ('. Libraries deployed')
   
  #
  # Deploy Applikations
  # print ('\nDeploy Applikations')
  deploy_Applications()    
  # print ('. Applikations deployed')

#
# ------------------------------------------------------------------------------
#

#
# Set Properties ...
print 'Set Properties'

fileName = '/tmp/' + domPrefix + '_Deploy.dat'
print ('. Data-/Parameter-File: ' + fileName )
 
scriptDir = os.path.dirname(sys.argv[0])

execfile(scriptDir + "/readConfigFile.py")
print ('. successfully insourcing of "' + scriptDir + '/readConfigFile.py"' )

domainsHome = '/u01/app/oracle/domains'
domainHome = domainsHome + '/' + domainName                                                                                                                                                    
scriptDir = domainHome + '/scripts/wls'

execfile(scriptDir + "/connectionUtil.py")
print ('. successfully insourcing of "' + scriptDir + '/connectionUtil.py"' )

execfile(scriptDir + "/wlsconfig/deployUtils.py")
print ('. successfully insourcing of "' + scriptDir + '/wlsconfig/deployUtils.py"' )

print ('. Domain environment is set now' )

#
# Update Starten
deploy_Libs_Apps(Environment)
exit()