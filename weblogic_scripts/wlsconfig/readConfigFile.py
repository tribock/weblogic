def readConfigFile(key):
  print('\nReading "' + key + '"-Records from File \"' + fileName + '\"' )                                                                                                                                             
  f = open(fileName)
  global lines
  lines = ''
  firstLine='y'
  try:                                                                                                                                                                                    
    for line in f:
      ### Strip the comment lines                                                                                                                                                         
      if line.strip().startswith('#'):
        continue
      else:                                                                                                                                                                                                                                                                                                                                      
        ### Split the blank seperated values                                                                                                                                              
        items = line.split(' | ')
        # print 'Anz items: ' + str(len(items))                                                                                                                                                                                                                                                                                                     
        if items[1]==key:
          # print ( '. Prozess Record: "' + line[0:48] + '..."' )                                                                                                                                                             
          print ( '. Typ: ' +items[1]+ ', ' +items[2]+ ', ' +items[3])
          line = line.replace("\n", "")
          if firstLine=='y':
            lines=line
            firstLine='n'
          else:
            lines=lines+';'+line                                                                                                                                                               
        else:                                                                                                                                                                             
          continue
  except Exception, e:      
    print "==>Error Occured"
    print e              
  f.close()
  # print ('Lines: ' +lines)
  return lines

