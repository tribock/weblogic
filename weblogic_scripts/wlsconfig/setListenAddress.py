###
# set ListenAddress of all Servers
###

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]
Environment=sys.argv[6]
CustomPrivateKeyPassPhrase=sys.argv[7]
if len(sys.argv) > 8:
  Cluster=sys.argv[8]
else:
  Cluster=""

import string
execfile(scriptDir + "/managedServerUtil.py")
execfile(scriptDir + "/connectionUtil.py")

def setCustomPrivateKeyAlias(srvName):
  try:
    cd('/Servers/'+srvName+'/NetworkAccessPoints/HTTPSInternalNetwork')
    listenAddr=cmo.getListenAddress()
    cmo.setChannelIdentityCustomized(true)
    cmo.setCustomPrivateKeyAlias(listenAddr)
    cmo.setCustomPrivateKeyPassPhrase(CustomPrivateKeyPassPhrase)
    print "CustomPrivateKeyAlias set to "+listenAddr + " for HTTPSInternalNetwork on " + srvName
  except Exception:
    print "Error while set Custom Keystore alias for " + srvName
  
def isInCluster(srvName, Cluster):
  srvName = srvName.replace('Server', 'Cluster')[:-1]
  if srvName == Cluster:
    return True
  return False

#get user home
from os.path import expanduser
home=expanduser("~")

#connect to Adminserver
connectToAdminServer(home, domName, admsrvHost, admsrvPort)
fragments=admsrvHost.split(".")
url=fragments[1]+'.'+fragments[2]+'.'+fragments[3]
edit()
startEdit()
for server in cmo.getServers():
  currenListenAddr=server.getListenAddress()
  srvName=server.getName()
  if len(Cluster) > 3:
    if not isInCluster(srvName, Cluster):
      continue
  setCustomPrivateKeyAlias(srvName)
  if url in currenListenAddr:
    print "already set " +currenListenAddr
    continue
  if srvName != domPrefix + '_AdminServer':
    fragments=admsrvHost.split(".")
    url=fragments[1]+'.'+fragments[2]+'.'+fragments[3]
    listenAddr="cn0"+srvName[-1:]+"."+url

  else:
    listenAddr=admsrvHost
    server.destroyNetworkAccessPoint(getMBean('/Servers/'+srvName+'/NetworkAccessPoints/HTTPSAdminNetwork'))
    server.destroyNetworkAccessPoint(getMBean('/Servers/'+srvName+'/NetworkAccessPoints/T3AdminNetwork'))

  print "change Listener for " + srvName + " to:"
  print(listenAddr)
  server.setListenAddress(listenAddr)
  cd('/')
  print "set SSL Private keys Alias for "+srvName + " to:"
  cd('/Servers/'+srvName+'/SSL/'+srvName)
  cmo.setServerPrivateKeyAlias(listenAddr)
  print cmo.getServerPrivateKeyAlias()

variable = raw_input('do you want to apply the changes?(yes): ')
print "you answered with "+ variable
if variable == "yes":
  activate()
else:
  undo()