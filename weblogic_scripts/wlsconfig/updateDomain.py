import sys
import socket
import os.path

domPrefix = sys.argv[1]
wlsVersion = sys.argv[2]
javaVersion = sys.argv[3]
scriptName = updateDomain

#
# ------------------------------------------------------------------------------
# defines local functions

def update_Domain(Environment):
  #
  # Connect to AdminServer 
  print ('\nConnect to AdminServer')
  home='/home/oracle'
  if Environment=='exa':
    home='/u01/home/oracle'  
  print ('+++ connectToAdminServer(' + home + ', ' + domainName + ', ' + adminListenAddress + ', ' + adminListenPort + ')')
  connectToAdminServer(home, domainName, adminListenAddress, adminListenPort)
  print ('. Connectet to AdminServer')
  
  # Redefine AdminServerLog
  print ('\nRedefine AdminServerLogs' )
  edit()
  startEdit()
  setServerLogValues(adminServerName, 62)
  print ('. AdminServerLog redefined' )


  # set SSL on AdminServer (if needed)
  if admSSL=='true':
    print ('\nSet SSL on AdminServer' )

    if identityStoreFile != '':
      cd('/Servers/' + adminServerName)
      cmo.setKeyStores('CustomIdentityAndCustomTrust')
      cmo.setCustomIdentityKeyStoreFileName(identityStoreFile)
      cmo.setCustomIdentityKeyStoreType('JKS')
      set('CustomIdentityKeyStorePassPhrase', identityStorePw)
      if trustStoreFile != '':        
        cmo.setCustomTrustKeyStoreFileName(trustStoreFile)
        cmo.setCustomTrustKeyStoreType('JKS')
        set('CustomTrustKeyStorePassPhrase', trustStorePw)

      cd('SSL/'+adminServerName)
      cmo.setEnabled(true)
      cmo.setServerPrivateKeyAlias(adminListenAddress)
      set('ServerPrivateKeyPassPhrase', trustStorePw)
      cmo.setHostnameVerificationIgnored(true)
      cmo.setHostnameVerifier(None)
      cmo.setTwoWaySSLEnabled(false)
      cmo.setClientCertificateEnforced(false)
      cmo.setListenPort(int(admSSLPort))
      print ('\nSet SSL on AdminServer done' )
  activate()
   
  #
  # Create Security Objects
  # print ('\nCreate Security Objects')
  create_SecurityObj(domainName)
  # print ('. Security Objects created')
  
  #
  # Create Provider
  # print ('\nCreate Provider (' + Providerlist + ')' )
  # print ('+++ "create_Providers" not started!' )
  create_Providers(Providerlist)
  # print ('. Providers created')

  #
  # Create Datasources
  # print ('\nCreate Datasources')
  create_Datasource()
  # print ('. Datasources created')

  #
  # Create LogFilter
  # print ('\nCreate LogFilter')
  SetLogFilter(logFilter)
  # print ('. LogFilter created')
  
  #
  # Create Cluster
  # print ('\nCreate Cluster')
  create_Cluster()
  # print ('. Cluster created')
  #
  # Create Server
  # print ('\nCreate Server')
  create_Server(identityStoreFile, identityStorePw, trustStoreFile, trustStorePw, adminListenAddress)
  # print ('. Server created')

  #
  # Target Datasources
  # print ('\nTarget Datasources')
  target_Datasources()
  # print ('. Datasources targeted')
   
  #
  # TLOG Settings
  # print ('\nSettings for TLOG')
  set_Tlog()
  # print ('. Settings for TLOG done')
   
  #
  # Create Jms
  # print ('\nCreate Jms')
  create_jms()
  # print ('. Jms created')
   
  #
  # Deploy Libraries
  # print ('\nDeploy Libraries')
  deploy_Libraries()
  # print ('. Libraries deployed')
   
  #
  # Deploy Applikations
  # print ('\nDeploy Applikations')
  deploy_Applications()    
  # print ('. Applikations deployed')

#
# ------------------------------------------------------------------------------
#

#
# Set Properties ...
print 'Set Properties'

fileName = '/tmp/' + domPrefix + '_Config.dat'
print ('. Data-/Parameter-File: ' + fileName )
scriptDir = os.path.dirname(sys.argv[0])

execfile(scriptDir + "/readConfigFile.py")
print ('. successfully insourcing of "' + scriptDir + '/readConfigFile.py"' )

# execfile(scriptDir + "/serverFunktions.py")
# print ('. successfully insourcing of "' + scriptDir + '/serverFunktions.py"' )

#
# Read and process 'DOM'-Record from file
readConfigFile('DOM')
domLines = lines.split(';')
print ('. Anz. DOM-Lines: ' + str(len(domLines)))
items = domLines[0].split(' | ')
(domShortName, typ, domainName, adminUser, adminPw, adminPort, ndmgrUser, ndmgrPw, identityStoreFile, identityStorePw, trustStoreFile, trustStorePw, logFilter, Providerlist, Filler) = items
print ('. DOM-Record: ' + domShortName + ', ' + typ + ', ' + domainName + ', ' + adminUser + ', ' + adminPw + ', ' + adminPort + ', ' + ndmgrUser + ', ' + ndmgrPw + ', ' + identityStoreFile + ', ' + identityStorePw + ', ' + trustStoreFile + ', ' + trustStorePw + ', ' + logFilter + ', ' + Providerlist)


host = socket.gethostname()
java_home = '/u01/app/oracle/product/' + javaVersion + '/jdk_' + domPrefix
wlHome = '/u01/app/oracle/product/' + wlsVersion + '/mwhome_' + domPrefix
templateHome = wlHome + '/wlserver/common/templates/wls'
domainTemplate = templateHome + '/wls.jar'
domainsHome = '/u01/app/oracle/domains'
domainHome = domainsHome + '/' + domainName
wlsHome = wlHome + '/wlserver'

domainHome = '/u01/app/oracle/domains/' + domainName
logHome = '/u01/app/oracle/logs/' + domainName                                                                                                                                                         
scriptDir = domainHome + '/scripts/wls'

execfile(scriptDir + "/connectionUtil.py")
print ('. successfully insourcing of "' + scriptDir + '/connectionUtil.py"' )

execfile(scriptDir + "/wlsconfig/createObjects.py")
print ('. successfully insourcing of "' + scriptDir + '/wlsconfig/createObjects.py"' )

execfile(scriptDir + "/wlsconfig/deployUtils.py")
print ('. successfully insourcing of "' + scriptDir + '/wlsconfig/deployUtils.py"' )

print ('. Domain environment is set now' )

#
# Read and prozess 'ADM'-Record from file
readConfigFile('ADM')
if lines!='':
  admLines = lines.split(';')
  print ('. Anz. ADM-Lines: ' + str(len(admLines)))
  items = admLines[0].split(' | ')
  (domShortName, typ, Fill1, adminServerName, adminListenPort, admSSL, admSSLPort, adminListenAddress, Filler) = items
  print ('. ADM-Record: ' + domShortName + ', ' + typ + ', ' + adminServerName + ', ' + adminPort + ', ' + admSSL + ', ' + admSSLPort + ', ' + adminListenAddress)
  #

  if len(admLines) < 4:
    adminPort = admSSLPort

#
# Set HW-Type (non-)exa
print ('\nSet HW-Type')
host = socket.gethostname()
if (host[:2]) == 'el':        
  Environment = 'exa'
  nmHost = (host[4:8]) + '-priv'
  numMachines = 4  
else:
  Environment = 'non-exa'
  nmHost = host
  numMachines = 1        
print('. Environment: ' + Environment)
localDomain = 'local.some_companyname.ch'

#
# Update Starten
update_Domain(Environment)
exit()