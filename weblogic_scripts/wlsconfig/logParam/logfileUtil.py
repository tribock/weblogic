
def SetValues(path,maxFile,FName ):
  cd( path )
  cmo.setFileCount( int(maxFile) )
  cmo.setFileName( FName )
  cmo.setFileTimeSpan( 24 )
  cmo.setLogFileRotationDir( '' )
  cmo.setRotateLogOnStartup( true )
  cmo.setRotationTime( '00:00' )
  cmo.setRotationType( 'byTime' )

def setServerLogValues(srvName, maxFile):
  print "setting for server '" + srvName + "' will be applied"
  path='/Servers/' + srvName + '/Log/' + srvName
  FName='/u01/app/oracle/logs/' + domName + '/' + srvName + '/' + srvName + '.log'
  SetValues(path,maxFile,FName )
  path='/Servers/' + srvName + '/WebServer/' + srvName + '/WebServerLog/' + srvName
  FName='/u01/app/oracle/logs/' + domName + '/' + srvName + '/' + srvName + '_access.log'
  SetValues(path,maxFile,FName )
  path='/Servers/' + srvName + '/DataSource/' + srvName + '/DataSourceLogFile/' + srvName
  FName='/u01/app/oracle/logs/' + domName + '/' + srvName + '/' + srvName + '_datasource.log'
  SetValues(path,maxFile,FName )
  cd('/Servers/' + srvName + '/ServerStart/' + srvName)
  cmo.setArguments('-Dweblogic.Stdout=/u01/app/oracle/logs/' + domName + '/' + srvName + '/' + srvName + '.out -Dweblogic.Stderr=/u01/app/oracle/logs/' + domName + '/' + srvName + '/' + srvName + '.err ')
  cd('/Servers/' + srvName + '/ServerDiagnosticConfig/' + srvName)
  cmo.setImageDir('/u01/app/oracle/logs/' + domName + '/' + srvName + '/' + srvName + '_diagnostic_images')
  cmo.setImageTimeout(1)
#