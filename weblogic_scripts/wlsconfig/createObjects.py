def create_Datasource(): 
  edit()
  startEdit()
  readConfigFile('JDB')
  if lines!='': 
          print ('. start createObjects.py->create_Datasource()') 
          jdbLines = lines.split(';')
          print ('. Anz. JDB-Lines: ' + str(len(jdbLines)))
          
          try:
            for iLines in range(0,len(jdbLines)):
              try:
                items = jdbLines[iLines].split(' | ')
                # print ('+++ JDBLine: ' +str(iLines)+ '  ' +jdbLines[iLines])
                # print ('+++ Anz items Datasouce: ' + str(len(items)))
                (domShortName, typ, clusterList, dbUser, dbPw, dbJndiName, dbDriver, dbInitSql, dbXa, dbUrl, gridLink, clientId, maxCapacity, minCapacity, statementCacheSize, Filler) = items
                # print (str(iLines)+ ' JDBC:   '+typ+ ', '+clusterList+ ', '+dbUser+ ', '+dbPw+ ', '+dbJndiName+ ', '+dbDriver+ ', '+dbInitSql+ ', '+dbXa+ ', '+dbUrl + ', ' + gridLink)    
                cd ('/')
                # print ('+++ ref = getMBean(/JDBCSystemResources/' + dbUser + ')')
                ref = getMBean('/JDBCSystemResources/' + dbUser)
                # print ('+++ ref = ' + str(ref))
                if ref != None:
                  getMBean('/JDBCSystemResources/' + dbUser)
                  print ('. JDBCSystemResource with name ' + dbUser + ' exists')
        
                else:  
                  cd('/')
                  print '. . Creating JDBCSystemResource with name ' + dbUser
                  cmo.createJDBCSystemResource(dbUser)

                cd('JDBCSystemResources/'+dbUser+'/JDBCResource/' + dbUser)
                cmo.setName(dbUser)
         
                cd('JDBCConnectionPoolParams/'+dbUser)
            
                cmo.setInitialCapacity(1)
                if maxCapacity == '':
                  cmo.setMaxCapacity(15)
                else:
                  cmo.setMaxCapacity(int(maxCapacity))
                if minCapacity == '':
                  cmo.setMinCapacity(1)
                else:
                  cmo.setMinCapacity(int(minCapacity))
                if statementCacheSize == '':
                  cmo.setStatementCacheSize(10)
                else:
                  cmo.setStatementCacheSize(int(statementCacheSize))
                cmo.setStatementCacheType('LRU')

                if clientId == 'true':
                  cmo.setCredentialMappingEnabled(true)
                cmo.setTestTableName('SQL SELECT 1 FROM DUAL')
                cmo.setInitSql(dbInitSql)
                                                                                                  
                cd('../..')
                cd('JDBCDataSourceParams/'+dbUser)
                if ref == None:
                  cmo.addJNDIName(dbJndiName)
                cmo.setGlobalTransactionsProtocol(dbXa)
               
                cd('../..')
                cd('JDBCOracleParams/'+dbUser)
           
                activeGridLink = false
                if gridLink == 'true':
                  activeGridLink = true 
                cmo.setActiveGridlink(activeGridLink )
                if clientId == 'true':
                  cmo.setUseDatabaseCredentials(true)
                             
                cd('../..')
                cd('JDBCDriverParams/'+dbUser)
           
                cmo.setUrl(dbUrl)
                cmo.setDriverName(dbDriver)
            
                esdb = encrypt(dbPw,domainHome)
                cmo.setPasswordEncrypted(esdb) 
                                     
                cd('Properties/'+dbUser)
                if ref == None:
                  cmo.createProperty('user') 
                                     
                cd('Properties/user')
                cmo.setValue(dbUser)
              except Exception ,e:
                print('exception ' +str(e))
                pass

              print ('. . configuring the data source ' + dbUser + ' done')
          #
            activate()
          except Exception ,e:
            print('exception ' +str(e))
            pass

# ------------------------------------------------------------------------------
def SetLogFilter(logFilter):
  print ('. start createObjects.py->SetLogFilter()') 
  if logFilter != '':
    edit() 
    startEdit()
    logFilters = logFilter.split('|')
    print ('. Anz. Logfilter: ' + str(len(logFilters))) 
    for iFilter in range(0, len(logFilters)):
      cd('/')
      logFilterFragments = logFilters[iFilter].split(':')
      logFilterName = logFilterFragments[0]
      logFilterContent = logFilterFragments[1]
      redirect ('/dev/null', 'false')
      ref = getMBean('/LogFilters/'+logFilterName)
      redirect ('/dev/null', 'true')
      if ref == None:  
        print ('. Define logfilter ' + logFilterName )
        cd('/')
        cmo.createLogFilter(logFilterName)
      else:
        print ('. Logfilter ' + logFilterName + ' found')
         
      cd('/LogFilters/'+logFilterName)
      cmo.setFilterExpression(logFilterContent) 
      print ('. Set filter rule ' + logFilterContent ) 
            
  else:
    print ('. No logfilters defined')  

# ------------------------------------------------------------------------------
def SetLogValues( path, maxFile, FName ):
  # print ('. start createObjects.py->SetLogValues()') 
  cd( path )
  cmo.setFileCount( int(maxFile) )
  cmo.setFileName( FName )
  cmo.setFileTimeSpan( 24 )
  cmo.setLogFileRotationDir( '' )
  cmo.setRotateLogOnStartup( true )
  cmo.setRotationTime( '00:00' )
  cmo.setRotationType( 'byTime' )

# ------------------------------------------------------------------------------
def setServerLogValues( srvName, maxFile ):
  print ('. start createObjects.py->setServerLogValues()') 
  print ('. setting for server ' + srvName + ' will be applied' )
  path='/Servers/' + srvName + '/Log/' + srvName
  FName=logHome + '/' + srvName + '/' + srvName + '.log'
  SetLogValues( path, maxFile, FName )
  print ('. . LogFile "' + FName + '" defined' )  
  path='/Servers/' + srvName + '/WebServer/' + srvName + '/WebServerLog/' + srvName
  FName=logHome + '/' + srvName + '/' + srvName + '_access.log'
  SetLogValues(path,maxFile,FName )
  print ('. . LogFile "' + FName + '" defined' )
  set('LogFileFormat', 'extended')  
  set('ELFFields', 'date time c-ip c-dns s-ip s-dns cs-uri cs-uri-stem cs-uri-query ctx-ecid ctx-rid cs-method sc-status sc-comment time-taken bytes')
  path='/Servers/' + srvName + '/DataSource/' + srvName + '/DataSourceLogFile/' + srvName
  FName=logHome + '/' + srvName + '/' + srvName + '_datasource.log'
  SetLogValues(path,maxFile,FName )
  print ('. . LogFile "' + FName + '" defined' )  
  cd('/Servers/' + srvName + '/ServerStart/' + srvName)
  cmo.setArguments('-Dweblogic.Stdout=' + logHome + '/' + srvName + '/' + srvName + '.out -Dweblogic.Stderr=' + logHome + '/' + srvName + '/' + srvName + '.err ')
  cd('/Servers/' + srvName + '/ServerDiagnosticConfig/' + srvName)
  cmo.setImageDir(logHome + '/' + srvName + '/' + srvName + '_diagnostic_images')
  cmo.setImageTimeout(1)
  print ('. . ImageDir "' + logHome + '/' + srvName + '/' + srvName + '_diagnostic_images" defined' )  

# ------------------------------------------------------------------------------
def createT3NetworkAP(newServerName, admsrvHost):
  print ('. start createObjects.py->createT3NetworkAP()')
  fragments=admsrvHost.split(".")
  url=fragments[1]+'.'+fragments[2]+'.'+fragments[3]
  listenAddr="cn0"+newServerName[-1:]+"-vip."+url
  cd('/')
  cd('/Servers/'+newServerName)
  cmo.createNetworkAccessPoint('T3ClientNetwork')

  cd('/Servers/'+newServerName+'/NetworkAccessPoints/T3ClientNetwork')
  cmo.setProtocol('t3')
  cmo.setListenAddress(listenAddr)
  cmo.setPublicAddress(listenAddr)
  cmo.setEnabled(true)
  cmo.setHttpEnabledForThisProtocol(true)
  cmo.setTunnelingEnabled(false)
  cmo.setOutboundEnabled(false)
  cmo.setTwoWaySSLEnabled(false)
  cmo.setClientCertificateEnforced(false)

# ------------------------------------------------------------------------------
def createHTTPSNetworkAP(newServerName, publicListenAddr, publicListenPort, internListenPort, identityStorePw):
  print ('. start createObjects.py->createHTTPSNetworkAP()')
  fragments=publicListenAddr.split(".")
  url=fragments[1]+'.'+fragments[2]+'.'+fragments[3]
  listenAddr="cn0"+newServerName[-1:]+"-priv"
  cd('/Servers/'+newServerName)
  cmo.setWeblogicPluginEnabled(true)
  cmo.createNetworkAccessPoint('HTTPSInternalNetwork')

  cd('/Servers/'+newServerName+'/NetworkAccessPoints/HTTPSInternalNetwork')
  cmo.setProtocol('https')
  cmo.setListenAddress(listenAddr)
  cmo.setListenPort(int(internListenPort))
  cmo.setPublicAddress(publicListenAddr)
  cmo.setPublicPort(int(publicListenPort))
  cmo.setEnabled(true)
  cmo.setHttpEnabledForThisProtocol(true)
  cmo.setTunnelingEnabled(false)
  cmo.setOutboundEnabled(false)
  cmo.setTwoWaySSLEnabled(false)
  cmo.setClientCertificateEnforced(false)
  cmo.setSDPEnabled(false)
  cmo.setChannelIdentityCustomized(true)
  cmo.setCustomPrivateKeyAlias(listenAddr)
  cmo.setCustomPrivateKeyPassPhrase(identityStorePw)

# ------------------------------------------------------------------------------
def create_Server(identityStoreFile , identityStorePw, trustStoreFile, trustStorePw, adminListenAddress):
  edit()
  startEdit()
  readConfigFile('SER')
  if lines!='':
          print ('. start createObjects.py->create_Server()')
          servLines = lines.split(';')
          print ('Anz. SER-Lines: ' +str(len(servLines)))
          
          for iLines in range(0,len(servLines)):
            listenAddress = ''
            # print ('Index: ' +str(iLines))
            items = servLines[iLines].split(' | ')
            # print ('SerLine: ' +servLines[iLines])
            # print ('Anz items Server: ' + str(len(items)))
            (domShortName, typ, ClusterName, serverName, listenPort, SSL, SSLPort, ListenAddr, TLOGPref, serverLogFilter, serverOutFileFilter, domainLogFileFilter, Filler) = items
            # print(str(iLines)+ ' Server: ' +domShortName+ ', ' +typ+ ', '+ClusterName+ ', '+serverName+ ', '+listenPort+ ', ' +SSL+ ', ' +SSLPort+ ', ' +ListenAddr+ ', ' +serverLogFilter+ ', ' +serverOutFileFilter+ ', ' +domainLogFileFilter+ ', ' +TLOGPref)
            
            ref = getMBean('/Servers/'+serverName)
            if ref==None:
              if Environment=='exa':
                if ListenAddr=='':
                  ListenAddr=adminListenAddress   
                fragments=adminListenAddress.split(".")
                url=fragments[1]+'.'+fragments[2]+'.'+fragments[3]
                listenAddress="cn0"+serverName[-1:]+"."+url        
                machineName=serverName.split('_')[0]+'_Machine0'+serverName[-1:]
              else:
                machineName=serverName.split('_')[0]+'_Machine01'
                
              clusterName=serverName.replace("Server", "Cluster")[:-1]
   
              if listenAddress =='':
                listenAddr = host + '.' + localDomain  
              cd('/')
              cmo.createServer(serverName)
              cd('/Servers/' + serverName)
              cmo.setListenAddress(listenAddress)
              cmo.setListenPort(int(listenPort))
              cmo.setStagingMode('nostage')
              set('MaxMessageSize',40000000)
              print ('. ' + str( getMBean('/Clusters/'+clusterName)))
              cmo.setCluster(getMBean('/Clusters/'+clusterName))
              cmo.setMachine(getMBean('/Machines/'+machineName))
              if SSL=='true': 
                sslPort=str(int(listenPort)+1)
                if identityStoreFile != '':
                  cmo.setKeyStores('CustomIdentityAndCustomTrust')
                  cmo.setCustomIdentityKeyStoreFileName(identityStoreFile)
                  cmo.setCustomIdentityKeyStoreType('JKS')
                  set('CustomIdentityKeyStorePassPhrase', identityStorePw)
                  if trustStoreFile != '':        
                    cmo.setCustomTrustKeyStoreFileName(trustStoreFile)
                    cmo.setCustomTrustKeyStoreType('JKS')
                    set('CustomTrustKeyStorePassPhrase', trustStorePw)
        
                  cd('SSL/'+serverName)
                  cmo.setEnabled(true)
                  cmo.setServerPrivateKeyAlias(listenAddress)
                  set('ServerPrivateKeyPassPhrase', trustStorePw)
                  cmo.setHostnameVerificationIgnored(true)
                  cmo.setHostnameVerifier(None)
                  cmo.setTwoWaySSLEnabled(false)
                  cmo.setClientCertificateEnforced(false)
                  cmo.setListenPort(int(sslPort))
        
              setServerLogValues(serverName, 62)
        
              if Environment=='exa':       
                createT3NetworkAP(serverName, adminListenAddress)
                if SSLPort != '':
                  internListenPort = str(int(SSLPort)-2000)   
                  createHTTPSNetworkAP(serverName, ListenAddr, SSLPort ,internListenPort, identityStorePw)       
        
            cd('/Servers/' +serverName+'/Log/'+serverName)     
            if serverLogFilter != '':
              cmo.setLogFileFilter(getMBean('/LogFilters/'+serverLogFilter ))
            if serverOutFileFilter != '':
              cmo.setStdoutFilter(getMBean('/LogFilters/'+serverOutFileFilter))
            if domainLogFileFilter != '':
              cmo.setDomainLogBroadcastFilter(getMBean('/LogFilters/'+domainLogFileFilter))
                   
          activate()  

# ------------------------------------------------------------------------------
def create_IdMProvider(domainName, idMUser, idMPw):
  print ('. start createObjects.py->create_IdMProvider()')
  AuthenticationProviderName='IDM_MetaDir_OpenLDAP'
  ref = getMBean('/SecurityConfiguration/' + domainName + '/Realms/myrealm/AuthenticationProviders/'+AuthenticationProviderName)
  if ref == None:
    print 'create idMProvider'
    cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm')
    cmo.createAuthenticationProvider(AuthenticationProviderName,'weblogic.security.providers.authentication.OpenLDAPAuthenticator')
    cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+AuthenticationProviderName)
    cmo.setControlFlag('SUFFICIENT')
    cmo.setHost('idm-metadir-p.intern.some_companyname')
    cmo.setPort(636)
    cmo.setSSLEnabled(true)
    cmo.setPrincipal('uid='+idMUser+',ou=accounts,ou=DSA,o=some_companyname-DIR,c=ch')
    cmo.setUserBaseDN('o=some_companyname-DIR,c=ch')
    cmo.setCredential(idMPw)
    cmo.setUserFromNameFilter('')
    cmo.setAllUsersFilter('(objectclass=person)')
    cmo.setUserNameAttribute('uid')
    cmo.setGroupBaseDN('o=some_companyname-DIR,c=ch')
    cmo.setGroupFromNameFilter('&(cn=%g)(objectclass=some_companynameGroup)')
    cmo.setConnectTimeout(120)
    cmo.setAllGroupsFilter('&(objectClass=some_companynameGroup)(|(cn=GG_WL_*)(cn=tax*))')
    cmo.setStaticGroupObjectClass('groupofnames')
    cmo.setConnectionPoolSize(10)
    cmo.setStaticMemberDNAttribute('member')
    cmo.setStaticGroupDNsfromMemberDNFilter('&(member=%M)(objectclass=groupofnames)')
    cmo.setGroupMembershipSearching('limited')
    cmo.setKeepAliveEnabled(true)

# ------------------------------------------------------------------------------
def create_eTaxProvider(domainName, userName, userPw):
  print ('. start createObjects.py->create_eTaxProvider()')
  AuthenticationProviderName='etaxesAuthenticationProvider'
  AuthenticationProviderType='ch.some_companyname.etaxes.authentication.wls.mbean.CustomEtaxesAuthenticationProvider'
  ref=getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+AuthenticationProviderName)
  if ref==None:
    cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm')
    cmo.createAuthenticationProvider(AuthenticationProviderName, AuthenticationProviderType)

  cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+AuthenticationProviderName)
  cmo.setControlFlag('SUFFICIENT')
  cmo.setDataSourceJNDIName('jdbc/etaxes')
  cmo.setPrincipalName(userName)
  cmo.setPrincipalPassword(userPw)

# ------------------------------------------------------------------------------
def create_Providers(Providerlist):
  edit()
  startEdit() 
  readConfigFile('PRO')
  if lines!='':
    print ('. start createObjects.py->create_Providers()')
    prvLines = lines.split(';')
    print ('Anz. PRO-Lines: ' +str(len(prvLines)))
    
    for iProv in range(0,len(prvLines)):
      items = prvLines[iProv].split(' | ')
      (domPrefix, typ, domainName, providerName , userName , userPw, Filler) = items
      print('. . check Provider :' + providerName + ' ' + str(iProv))
      if providerName=='IDM_MetaDir_OpenLDAP': 
        create_IdMProvider(domainName, userName, userPw)
      if providerName=='etaxesAuthenticationProvider':
        print('. etaxesAuthenticationProvider: ' + str(iProv))  
        create_eTaxProvider(domainName, userName, userPw)

    cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
    cmo.setControlFlag('SUFFICIENT')
    
    providers = Providerlist.split('|')
    print ('. Anz. Providers: ' + str(len(providers)))

    for iProv in range(0,len(providers)):
#       print ('. . ProviderIndex: ' +str(iProv))     
      print ('. . provMBean'+str(iProv)+' = getMBean(/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv]+')')
      if iProv == 0:
        try: 
           provMBean1 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv == 1:
        try:
           provMBean2 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv == 2:
        try:
           provMBean3 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv == 3:
        try:
           provMBean4 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv == 4:
        try:
           provMBean5 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv == 5:
        try:
           provMBean6 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv == 6:
        try:
           provMBean7 = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/'+providers[iProv])
        except Exception, e:
           print('exception ' +str(e))   
      if iProv  > 6:
        print('. Error mehr als 7 Security Provider! Reihenfolge muss manuell angepasst werden')


    realm = getMBean('/SecurityConfiguration/'+domainName+'/Realms/myrealm')
    if len(providers) == 2:
      realm.setAuthenticationProviders(jarray.array([provMBean1,provMBean2], weblogic.management.security.authentication.AuthenticationProviderMBean))
    if len(providers) == 3:
      realm.setAuthenticationProviders(jarray.array([provMBean1,provMBean2,provMBean3], weblogic.management.security.authentication.AuthenticationProviderMBean))
    if len(providers) == 4:
      realm.setAuthenticationProviders(jarray.array([provMBean1,provMBean2,provMBean3,provMBean4], weblogic.management.security.authentication.AuthenticationProviderMBean))
    if len(providers) == 5:
      realm.setAuthenticationProviders(jarray.array([provMBean1,provMBean2,provMBean3,provMBean4,provMBean5], weblogic.management.security.authentication.AuthenticationProviderMBean))
    if len(providers) == 6:
      realm.setAuthenticationProviders(jarray.array([provMBean1,provMBean2,provMBean3,provMBean4,provMBean5,provMBean6], weblogic.management.security.authentication.AuthenticationProviderMBean))
    if len(providers) == 7:
      realm.setAuthenticationProviders(jarray.array([provMBean1,provMBean2,provMBean3,provMBean4,provMBean5,provMBean6,provMBean7], weblogic.management.security.authentication.AuthenticationProviderMBean))

  # change Pw rules to default)
  cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/PasswordValidators/SystemPasswordValidator')
  cmo.setMinPasswordLength(8)
  cmo.setMinNumericOrSpecialCharacters(1)
  cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
  cmo.setMinimumPasswordLength(8)

  activate()

# ------------------------------------------------------------------------------
def create_User(domainName, newUserName, newUserPassword, newUserDescription):
    cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
    if (cmo.userExists(newUserName)):
        # cannot create !!
        print 'User '+newUserName+' already exists - CANNOT create !'
        pass
    else:

      try:
        cmo.createUser(newUserName, newUserPassword, newUserDescription)
        print '. . User ' + newUserName + ' created'
      except Exception, e:
        print '. . Error creating User ' + newUserName + str(e)
        

# ------------------------------------------------------------------------------
def create_Group(domainName, newGroupName, newGroupDescription, newGroupUser):
  try:
     cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')

     try:
        cmo.createGroup(newGroupName, newGroupDescription)
        print '. . Group ' + newGroupName + ' created'
     except:
        print '. . Group ' + newGroupName + ' already exists'
        
     cmo.addMemberToGroup(newGroupName,newGroupUser)

  except Exception, e:
    print('exception ' + str(e))
    pass

# ------------------------------------------------------------------------------
def create_Role(domainName, newRoleName, newRoleExpression):
  try:
     cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/RoleMappers/XACMLRoleMapper')

     try:
        cmo.createRole(None,newRoleName, None)
        print '. . Role ' + newRoleName + ' created'
     except:
        print '. . Role ' + newRoleName + ' already exists'        
             
     cmo.setRoleExpression(None,newRoleName,newRoleExpression)
  except Exception, e:
    print('exception ' +str(e))
    pass

# ------------------------------------------------------------------------------
def create_SecurityObj(domainName):
  serverConfig()  
  userLines=''
  readConfigFile('USR')
  if lines!='':
    userLines = lines.split(';')
    print ('Anz. USR-Lines: ' +str(len(userLines)))
  
  groupLines='' 
  readConfigFile('GRP')
  if lines!='':
    groupLines = lines.split(';')
    print ('Anz. GRP-Lines: ' +str(len(groupLines)))

  roleLines=''
  readConfigFile('ROL')
  if lines!='':  
    roleLines = lines.split(';')
    print ('Anz. ROL-Lines: ' +str(len(roleLines)))
  
  try:
    if userLines!='':
      print ('\n. start creating users ...')
      # change Pw rules (less restrictions)

      edit()
      startEdit()   
      cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/PasswordValidators/SystemPasswordValidator')
      cmo.setMinPasswordLength(5)
      cmo.setMinNumericOrSpecialCharacters(0)
      cd('/SecurityConfiguration/'+domainName+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
      cmo.setMinimumPasswordLength(5)
      activate()

      serverConfig()
      for iLines in range(0,len(userLines)):
        items = userLines[iLines].split(' | ')
        (domShortName, typ, userName, userPw, userDescription, groupsOfUser, Filler) = items
#        print(str(iLines)+ ' User:   '+typ+ ', '+userName+ ', '+userPw+ ', '+userDescription)
        create_User(domainName, userName, userPw, userDescription)
      print ('. users created')

    if groupLines!='':
      print ('\n. start creating groups ...')
      for iLines in range(0,len(groupLines)):
        items = groupLines[iLines].split(' | ')
        (domShortName, typ, groupName, groupDescription, groupMembers, Filler) = items
#        print(str(iLines)+ ' Group:   '+typ+ ', '+groupName+ ', '+groupDescription+ ', '+groupMembers)
        create_Group(domainName, groupName, groupDescription, groupMembers)
      print ('\n. groups created')

    if roleLines!='':
      print ('\n. start creating roles ...')
      for iLines in range(0,len(roleLines)):
        items = roleLines[iLines].split(' | ')
        (domShortName, typ, roleName, roleExpression, Filler) = items
#        print(str(iLines)+ ' Role:   '+typ+ ', '+roleName+ ', '+roleExpression)
        create_Role(domainName, roleName, roleExpression)
      print ('\n. roles created')

  except Exception, e:
    print('exception ' +str(e))
    pass
 

# ------------------------------------------------------------------------------
def create_Cluster():  
  edit()
  startEdit()
  readConfigFile('SER')
  if lines!='':
    print ('. start createObjects.py->create_Cluster()')
    servLines = lines.split(';')
    print ('Anz. SER-Lines: ' +str(len(servLines)))
    for iLines in range(0,len(servLines)):
      items = servLines[iLines].split(' | ')
      (domShortName, typ, ClusterName, serverName, listenPort, SSL, SSLPort, ListenAddr, TLOGPref, serverLogFilter, serverOutFileFilter, domainLogFileFilter, Filler) = items
      # print(str(iLines)+ ' Server: ' +domShortName+ ', ' +typ+ ', '+ClusterName+ ', '+serverName+ ', '+listenPort+ ', ' +SSL+ ', ' +SSLPort+ ', ' +ListenAddr+ ', ' +serverLogFilter+ ', ' +serverOutFileFilter+ ', ' +domainLogFileFilter+ ', ' +TLOGPref)
    
#     ref = getMBean('/Clusters/'+ClusterName)
#     if ref==None:
      try:
        print ('. . creating Cluster ' + ClusterName)
        cd('/')
        cmo.createCluster(ClusterName)
        cd('/Clusters/' + ClusterName)
        cmo.setClusterMessagingMode('unicast')
        print ('. . Cluster ' + ClusterName + ' created')
      except:  
        print ('. . Cluster ' + ClusterName + ' already exists')

    activate()    

# ------------------------------------------------------------------------------
def set_Tlog(): 
  edit()
  startEdit()
  readConfigFile('SER')
  if lines!='':
    print ('. start createObjects.py->set_Tlog()')
    servLines = lines.split(';')
    print ('Anz. SER-Lines: ' +str(len(servLines)))
          
    for iLines in range(0,len(servLines)):
      items = servLines[iLines].split(' | ')
      # print ('SerLine: ' +servLines[iLines])
      # print ('Anz items Server: ' + str(len(items)))
      (domShortName, typ, ClusterName, serverName, listenPort, SSL, SSLPort, ListenAddr, TLOGPref, serverLogFilter, serverOutFileFilter, domainLogFileFilter, Filler) = items
      # print(str(iLines)+ ' Server: ' +domShortName+ ', ' +typ+ ', '+ClusterName+ ', '+serverName+ ', '+listenPort+ ', ' +SSL+ ', ' +SSLPort+ ', ' +ListenAddr+ ', ' +serverLogFilter+ ', ' +serverOutFileFilter+ ', ' +domainLogFileFilter+ ', ' +TLOGPref)
            
      tlogName=serverName.replace("Server", "")[4:-2]
      tlogDsName=tlogName.upper()+'TLOG'
        
      print('. TLOG: ' + tlogDsName)

      cd('/Servers/'+serverName+'/TransactionLogJDBCStore/'+serverName)
      
      print ('+++ ' + str( getMBean('/JDBCSystemResources/' + tlogDsName )))
      print ('+++ CMD: cmo.setDataSource( getMBean("/JDBCSystemResources/' + tlogDsName + '"))')
      tlogDs = getMBean('/JDBCSystemResources/' + tlogDsName)
      if tlogDs != None:
        cmo.setEnabled(true)
        # print ('+++ Not executed: "cmo.setDataSource(getMBean(/JDBCSystemResources/' + tlogDsName +')"')
        cmo.setDataSource(getMBean('/JDBCSystemResources/' + tlogDsName))
      cmo.setPrefixName(TLOGPref)

# ------------------------------------------------------------------------------
def target_Datasources(): 
  edit()
  startEdit()
  readConfigFile('JDB')
  if lines!='':
          print ('. start createObjects.py->target_Datasources()')
          jdbLines = lines.split(';')
          print ('Anz. JDB-Lines: ' +str(len(jdbLines)))
          
          try:
            for iLines in range(0,len(jdbLines)):
              items = jdbLines[iLines].split(' | ')
              (domShortName, typ, clusterList, dbUser, dbPw, dbJndiName, dbDriver, dbInitSql, dbXa, dbUrl, gridLink, clientId, maxCapacity, minCapacity, statementCacheSize, Filler) = items
              # print(str(iLines)+ ' JDBC:   '+typ+ ', '+clusterList+ ', '+dbUser)
              if clusterList!='':
                cd('/JDBCSystemResources/'+dbUser)
                datasource=cmo
                clusterNames = clusterList.split(',')
                for iTarget in range(0,len(clusterNames)):
                  print('. target Datasource ' +dbUser+ ' to Cluster ' +clusterNames[iTarget])
                  cd('/Clusters/'+clusterNames[iTarget])        
                  dstarget = cmo
                  datasource.addTarget(dstarget)
        
          except Exception ,e:
            print('exception ' +str(e))
            pass
          activate() 
#

# ------------------------------------------------------------------------------
def create_JMSStoreAndServer(jmsStoreName, jmsDsName, jmsPrefixName, jmsServerName, targetDir): 
  print ('. start createObjects.py->create_JMSStoreAndServer()')
  
  try:
    ref = getMBean('/JDBCStores/'+jmsStoreName)
    if ref==None:
      cd('/')
      cmo.createJDBCStore(jmsStoreName)
    cd('/JDBCStores/'+jmsStoreName)
    cmo.setDataSource(getMBean('/JDBCSystemResources/'+jmsDsName))
    cmo.setPrefixName(jmsPrefixName)
    jmsStore=cmo
    cd(targetDir)
    target=cmo
    jmsStore.addTarget(target)
           
    ref = getMBean('/JMSServers/'+jmsServerName)
    if ref==None:
      cd('/')
      cmo.createJMSServer(jmsServerName)
    cd('/JMSServers/'+jmsServerName)
    cmo.setPersistentStore(getMBean('/JDBCStores/'+jmsStoreName))
    cmo.setTemporaryTemplateResource(None)
    cmo.setTemporaryTemplateName(None)
    jmsServer=cmo
    cd(targetDir)
    target=cmo
    jmsServer.addTarget(target)
    path='/JMSServers/'+jmsServerName+'/JMSMessageLogFile/'+jmsServerName
    maxFile=62
    FName=logHome + '/' + jmsServerName + '/' + jmsServerName + '.log'
    SetLogValues( path, maxFile, FName )

  except Exception ,e:
    print('exception ' +str(e))
    pass
#

# ------------------------------------------------------------------------------
def createJMSModule(jmsModuleName, clusterName, jmsConnectionFactoryName, jmsQuotaName, jmsQuotaMaxBytes, jmsQuotaMaxMsg, jmsQueueTyp, jmsQueueName, jmsErrorQueueName, jmsRedeliveryDelay, jmsRedeliveryLimit, jmsTimeToDeliver):
  print ('. start createObjects.py->createJMSModule()')
  
  try:
    ref = getMBean('/JMSSystemResources/'+jmsModuleName)
    if ref==None:
      cd('/')
      cmo.createJMSSystemResource(jmsModuleName, 'jms/'+jmsModuleName+'-jms.xml')
    cd('/JMSSystemResources/'+jmsModuleName)
    jmsModule=cmo
    cd('/Clusters/'+clusterName)
    targetCluster=cmo
    jmsModule.addTarget(targetCluster)
    cd('/JMSSystemResources/'+jmsModuleName)
    jmsSubDeploymentName=jmsModuleName.replace('Module','Subdeployment')
    ref = getMBean('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+jmsSubDeploymentName)
    if ref==None:
      cmo.createSubDeployment(jmsSubDeploymentName)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName)
    ref = getMBean('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/ConnectionFactories/'+jmsConnectionFactoryName)
    if ref==None:
      cmo.createConnectionFactory(jmsConnectionFactoryName)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/ConnectionFactories/'+jmsConnectionFactoryName)
    cmo.setJNDIName('jms/'+jmsConnectionFactoryName)
    cmo.setDefaultTargetingEnabled(true)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/ConnectionFactories/'+jmsConnectionFactoryName+'/SecurityParams/'+jmsConnectionFactoryName)
    cmo.setAttachJMSXUserId(false)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/ConnectionFactories/'+jmsConnectionFactoryName+'/ClientParams/'+jmsConnectionFactoryName)
    cmo.setClientIdPolicy('Restricted')
    cmo.setSubscriptionSharingPolicy('Exclusive')
    cmo.setMessagesMaximum(10)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/ConnectionFactories/'+jmsConnectionFactoryName+'/TransactionParams/'+jmsConnectionFactoryName)
    cmo.setXAConnectionFactoryEnabled(true)
    if jmsQuotaName != '':
      cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName)
      ref = getMBean('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/Quotas/'+jmsQuotaName)
      if ref==None:
        cmo.createQuota(jmsQuotaName)
      cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/Quotas/'+jmsQuotaName)
      cmo.setBytesMaximum(int(jmsQuotaMaxBytes))
      cmo.setMessagesMaximum(int(jmsQuotaMaxMsg))
      cmo.setPolicy('FIFO')
      cmo.setShared(false)
    if jmsErrorQueueName!='':
      cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName)
      ref = getMBean('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsErrorQueueName)
      if ref==None:
        if jmsQueueTyp == 'UniformDistributedQueues': 
          cmo.createUniformDistributedQueue(jmsErrorQueueName)
        else:
          cmo.createQueue(jmsErrorQueueName)
      cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsErrorQueueName)
      cmo.setJNDIName('jms/'+jmsErrorQueueName)
      cmo.unSet('Template')
      cmo.setDefaultTargetingEnabled(false)
      cmo.setSubDeploymentName(jmsSubDeploymentName)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName)
    ref = getMBean('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsQueueName)
    if ref==None:
      if jmsQueueTyp == 'UniformDistributedQueues':
        cmo.createUniformDistributedQueue(jmsQueueName)
      else:      
        cmo.createQueue(jmsQueueName)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsQueueName)
    cmo.setJNDIName('jms/'+jmsQueueName)
    cmo.unSet('Template')
    cmo.setDefaultTargetingEnabled(false)
    cmo.setSubDeploymentName(jmsSubDeploymentName)
    cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsQueueName+'/DeliveryFailureParams/'+jmsQueueName)      
    if jmsRedeliveryLimit!='0':
      if jmsRedeliveryLimit!='':
        cmo.setRedeliveryLimit(int(jmsRedeliveryLimit))  
    if jmsErrorQueueName!='':  
      cmo.setExpirationPolicy('Redirect')
      cmo.setErrorDestination(getMBean('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsErrorQueueName))
    if jmsRedeliveryDelay!='':
      cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsQueueName+'/DeliveryParamsOverrides/'+jmsQueueName)
      cmo.setRedeliveryDelay(int(jmsRedeliveryDelay))
    if jmsTimeToDeliver!='':
      timeToDeliver=jmsTimeToDeliver.replace(":",";")
      cd('/JMSSystemResources/'+jmsModuleName+'/JMSResource/'+jmsModuleName+'/'+jmsQueueTyp+'/'+jmsQueueName+'/DeliveryParamsOverrides/'+jmsQueueName)
      cmo.setDeliveryMode('No-Delivery')
      cmo.setTimeToDeliver(timeToDeliver)

  except Exception ,e:
    print('exception ' +str(e))
    pass
#

# ------------------------------------------------------------------------------
def create_jms():  
  edit()
  startEdit()
  readConfigFile('JMS')
  if lines!='':
          print ('. start createObjects.py->create_jms()')
          jmsLines = lines.split(';')
          print ('Anz. JMS-Lines: ' +str(len(jmsLines)))
        
          try:
            for iLines in range(0,len(jmsLines)):
              items = jmsLines[iLines].split(' | ')
              (domShortName, typ, clusterName, jmsModuleName, jmsQueueName, jmsConnectionFactoryName, jmsErrorQueueName, jmsQuotaName, jmsQuotaMaxBytes, jmsQuotaMaxMsg, jmsMigratableTarget, jmsRedeliveryDelay, jmsRedeliveryLimit, jmsTimeToDeliver, Filler) = items
              # print(str(iLines)+ ' JMS:   '+typ+ ', ' +clusterName+ ', ' +jmsModuleName+ ', ' +jmsQueueName+ ', ' +jmsConnectionFactoryName+ ', ' +jmsErrorQueueName+ ', ' +jmsQuotaName+ ', ' +jmsQuotaMaxBytes+ ', ' +jmsQuotaMaxMsg+ ', ' +jmsMigratableTarget+ ', ' +jmsRedeliveryDelay+ ', ' +jmsRedeliveryLimit)  
              cd('/')
              jmsDsName=clusterName.replace("Cluster", "jms")[4:-1]
              jmsDsName=jmsDsName.upper()
              clusterServers = []
              servers = cmo.getServers()
              for server in servers:
                if server.getCluster()!=None and server.getCluster().getName()==clusterName:
                  clusterServers.append(server.getName())
              if jmsMigratableTarget=='':
                for ServerName in clusterServers:
                  jmsSuffix=ServerName[len(ServerName)-2]
                  jmsStoreName=ServerName.replace('Server','JmsStore')
                  jmsServerName=ServerName.replace('Server','JmsServer')
                  jmsPrefixName='JMS_'+ServerName.replace('Server','S')+'_'
                  jmsQueueTyp = 'UniformDistributedQueues'
                  targetDir = '/Servers/'+ServerName
                  create_JMSStoreAndServer(jmsStoreName, jmsDsName, jmsPrefixName, jmsServerName, targetDir)           

                # print ('. . JMSModul ' + jmsModuleName + 'jmsQueue ' + jmsQueueName + ' for cluster ' + clusterName + ' start creating/updating ...' )        
                createJMSModule(jmsModuleName, clusterName, jmsConnectionFactoryName, jmsQuotaName, jmsQuotaMaxBytes, jmsQuotaMaxMsg, jmsQueueTyp, jmsQueueName, jmsErrorQueueName, jmsRedeliveryDelay, jmsRedeliveryLimit, jmsTimeToDeliver)
                print ('. . JMSModul ' + jmsModuleName + 'JMSQueue ' + jmsQueueName + ' for cluster ' + clusterName + ' created/updated' )
                jmsSubDeploymentName=jmsModuleName.replace('Module','Subdeployment')
                cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+jmsSubDeploymentName)
                subdeployment=cmo
                for ServerName in clusterServers:
                  jmsServerName=ServerName.replace('Server','JmsServer')
                  cd('/JMSServers/'+jmsServerName)
                  target=cmo
                  subdeployment.addTarget(target)      
              else:
                jmsStoreName=jmsModuleName.replace('Module','Store')
                jmsServerName=jmsModuleName.replace('Module','Server')
                jmsPrefixName='JMS_'+jmsModuleName.replace('JMSModule','')+'_'
                jmsQueueTyp = 'Queues'
                targetDir = '/MigratableTargets/'+jmsMigratableTarget+' (migratable)'
                 
                create_JMSStoreAndServer(jmsStoreName, jmsDsName, jmsPrefixName, jmsServerName, targetDir)           
        
                createJMSModule(jmsModuleName, clusterName, jmsConnectionFactoryName, jmsQuotaName, jmsQuotaMaxBytes, jmsQuotaMaxMsg, jmsQueueTyp, jmsQueueName, jmsErrorQueueName, jmsRedeliveryDelay, jmsRedeliveryLimit)    
                print ('. . Migratable JMSModule ' + jmsModuleName + 'JMSQueue ' + jmsQueueName + ' for cluster ' + clusterName + ' created/updated' )
                jmsSubDeploymentName=jmsModuleName.replace('Module','Subdeployment')
                cd('/JMSSystemResources/'+jmsModuleName+'/SubDeployments/'+jmsSubDeploymentName)
                subdeployment=cmo
                cd('/JMSServers/'+jmsServerName)
                target=cmo
                subdeployment.addTarget(target) 
        
          except Exception ,e:
            print('exception ' +str(e))
            pass
          activate() 