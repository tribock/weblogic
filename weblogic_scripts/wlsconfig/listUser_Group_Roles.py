###
# List users, groups and roles
###

import sys
from os.path import expanduser
from weblogic.management.security.authentication import UserReaderMBean

#fill in variables
domName=sys.argv[1]
domPrefix=sys.argv[2]
admsrvHost=sys.argv[3]
admsrvPort=sys.argv[4]
scriptDir=sys.argv[5]

execfile(scriptDir + "/managedServerUtil.py")
execfile(scriptDir + "/connectionUtil.py")

#get user home
home = expanduser("~")

connectToAdminServer(home, domName, admsrvHost, admsrvPort)

selUserName="*"
maxOutLines=1000

realmName=cmo.getSecurityConfiguration().getDefaultRealm()
authProviderU = realmName.getAuthenticationProviders()

# ------------------------------------------------------------
# --  User und Gruppen auslesen ...  -------------------------
# ------------------------------------------------------------

for i in authProviderU:
    if isinstance(i,UserReaderMBean):
        userName  = i
        userGroup = i
        userRole  = i
        authName  = i.getName()

        if authName == 'DefaultAuthenticator':
            userList = i.listUsers(str(selUserName),int(maxOutLines))
            while userName.haveCurrent(userList):
                localUserName = userName.getCurrentName(userList)
                localUserDesc = userName.getUserDescription(localUserName)
                c_grp=i.listMemberGroups(userName.getCurrentName(userList))
                localUserGroup = ""
                while userGroup.haveCurrent(c_grp):
                    localUserGroup = localUserGroup + userGroup.getCurrentName(c_grp) + ' ; '
                    userGroup.advance(c_grp)
                if localUserName == '' or localUserName == None:
                   localUserName = "(undefined)"
                if localUserDesc == '' or localUserDesc == None:
                   localUserDesc = "(undefined)"
                if localUserGroup == '' or localUserGroup == None:
                   localUserGroup = "(undefined)"
                print('1 - U ; ' + localUserName  + ' ; ' + localUserDesc + ' ; ' + localUserGroup)
                if  localUserGroup != '':
                   print('2 - G ; ' + localUserGroup + ' ; ' + localUserName + ' ; ' + localUserDesc)
                userName.advance(userList)
            userName.close(userList)


# ------------------------------------------------------------
# --  Rollen auslesen ...  -----------------------------------
# ------------------------------------------------------------

authorizer=realmName.lookupRoleMapper("XACMLRoleMapper")

roleList = authorizer.listAllRoles(int(maxOutLines))

userReader = authorizer
while userReader.haveCurrent(roleList):
    usrrd = userReader.getCurrentProperties(roleList)
    localResourceId = usrrd.get('ResourceId')
    if localResourceId == '' or localResourceId == None:
       localRoleName = usrrd.get('RoleName')
       localExpression = usrrd.get('Expression')
       if localRoleName != "**" and localExpression != "":
           print('3 - R ; ' + localRoleName + ' ; ' + localExpression)
    userReader.advance(roleList)

disconnect()
