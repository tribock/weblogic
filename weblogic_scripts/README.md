simplified weblogic scripts for basic maintenance

# Installation Gitlab Runner
## LNXSRV17

- login as root
- create user gitlab `useradd gitlab -d /u01/home/gitlab/`
- add gitlab runner to sudo command 
```
visudo

## Allow Gitlab runner to run gitlab-runner as root
gitlab ALL=/bin/gitlab-runner ALL
```
add a new feature