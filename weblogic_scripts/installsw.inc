#!/bin/bash
# ------------------------------------------------------------
# -- install jdk and WLS Software                           --
# ------------------------------------------------------------
        echo "Beginn 'installsw.inc'"

         # check jdk
         PRODUCT_INSTALL_DIR=/u01/app/oracle/product
         export JAVA_HOME=${PRODUCT_INSTALL_DIR}/${javaVersion}/jdk_${prefix}
         if [[ ! -d ${JAVA_HOME} ]]
         then
            mkdir -p ${JAVA_HOME}                                              2>&1 | tee -a ${logFile}
            echo "New Directory '${JAVA_HOME}' createtd"                       2>&1 | tee -a ${logFile}
         fi
         
         # install jdk if needed
         jdkInstall="N"
         cd ${JAVA_HOME}    
         echo "changed into Directory `pwd`"                                   2>&1 | tee -a ${logFile}                                                                                                             
         if [[ -d ${JAVA_HOME}/bin ]]; then
             # check for right version
             echo "Directory '${JAVA_HOME}/bin' found"                         2>&1 | tee -a ${logFile}  
             ./bin/java -version                                               2>&1 | tee -a ${logFile}
             ./bin/java -version 2>&1 | grep -i version >/tmp/jversion.txt     2>&1 | tee -a ${logFile}
             echo "Installed: `cat /tmp/jversion.txt`"                         2>&1 | tee -a ${logFile} 
             jVers=`cat /tmp/jversion.txt | cut -f2 -d'"'`
             jVers=${jVers//'0_'/''}
             if [[ "${jVers}" == "${javaVersion}" ]]; then
                 echo "jdk${jVers} already installed"                          2>&1 | tee -a ${logFile}
             else
                 # delete old version
                 echo "delete old jdk jdk_${prefix}"                           2>&1 | tee -a ${logFile}
                 cd ${JAVA_HOME}
                 rm * -rf                                                      2>&1 | tee -a ${logFile}
                 jdkInstall="Y"
             fi
         else jdkInstall="Y"
         fi
         echo "jdkInstall set to '${jdkInstall}'"
         
         if [[ "${jdkInstall}" == "Y" ]]; then
             cd ${JAVA_HOME}
             echo "install new jdk ${javaVersion} in `pwd`"                    2>&1 | tee -a ${logFile}                                                                                                             
             jdkVers=`echo ${javaVersion} | awk -F"." '{print ($2 "u" $3)}'`
             echo "Cmd: tar -xzvf /u01/common/general/jdk/jdk-${jdkVers}-linux-x64.tar.gz" >>${logFile}
             tar -xzvf /u01/common/general/jdk/jdk-${jdkVers}-linux-x64.tar.gz 2>&1 >/var/tmp/tmp.log 
             lRCD=$?
             cat /var/tmp/tmp.log | awk '{print (" | " $0) }' >>${logFile}
             if [ ${lRCD} -eq 0 ]
             then
                echo "Install jdk ${javaVersion} OK"                           2>&1 | tee -a ${logFile}
             else
                echo "Install jdk ${javaVersion} with Error '${lRCD}'"         2>&1 | tee -a ${logFile}
                cat /var/tmp/tmp.log | awk '{print (" | " $0) }'
                exit ${lRCD}
             fi 
             extractDir=`ls`
             cd ${extractDir}
             mv ./* ../                                                        2>&1 | tee -a ${logFile}
             cd ..
             rmdir ${extractDir}                                               2>&1 | tee -a ${logFile}
         fi
         
         echo ""
         # check wls
         export MW_HOME=${PRODUCT_INSTALL_DIR}/${wlsVersion}/mwhome_${prefix}
         if [[ ! -d ${MW_HOME} ]]
         then
            mkdir -p ${MW_HOME}                                                2>&1 | tee -a ${logFile}
            echo "New Directory '${MW_HOME}' createtd"                         2>&1 | tee -a ${logFile}
         fi

         # install wls if needed         
         wlsInstall="N"           
         cd ${MW_HOME}
         echo "changed into Directory `pwd`"                                   2>&1 | tee -a ${logFile}                                                                                                             

         if [[ -d $MW_HOME/wlserver ]]; then
             # check for right version
             echo "Directory '${MW_HOME}/wlserver' found"                      2>&1 | tee -a ${logFile}  
             
             versLen=${#wlsVersion}
             wlVers=`grep 'WebLogic Server' ${MW_HOME}/inventory/registry.xml | cut -f6 -d'"' | cut -c1-${versLen}`
             if [[ "${wlVers}" != "${wlsVersion}" ]]; then
                 # delete old version(s)
                 if [[ -d $MW_HOME/otd ]]; then
                     echo "delete old otd ${wlVers}"                           2>&1 | tee -a ${logFile}
                     echo "Cmd: $MW_HOME/oui/bin/deinstall.sh -silent -distributionName 'Oracle Traffic Director' -distributionVersion ${wlVers}" >>${logFile}
                     $MW_HOME/oui/bin/deinstall.sh -silent -distributionName "Oracle Traffic Director" -distributionVersion ${wlVers} 2>&1 >/var/tmp/tmp.log
                     lRCD=$?
                     cat /var/tmp/tmp.log | awk '{print (" | " $0) }' >>${logFile}
                     if [ ${lRCD} -eq 0 ]
                     then
                        echo "delete otd ${wlVers} OK"                         2>&1 | tee -a ${logFile}
                     else
                        echo "delete otd ${wlVers} with Error '${lRCD}'"       2>&1 | tee -a ${logFile}
                        cat /var/tmp/tmp.log | awk '{print (" | " $0) }'
                        exit ${lRCD}
                     fi 
                 fi
                 echo "delete old wls ${wlVers}"                               2>&1 | tee -a ${logFile} 
                 echo "Cmd: $MW_HOME/oui/bin/deinstall.sh -silent -distributionName 'WebLogic Server for FMW' -distributionVersion ${wlVers}" >>${logFile}
                 $MW_HOME/oui/bin/deinstall.sh -silent -distributionName "WebLogic Server for FMW" -distributionVersion ${wlVers} 2>&1 >/var/tmp/tmp.log
                 lRCD=$?
                 cat /var/tmp/tmp.log | awk '{print (" | " $0) }' >>${logFile}
                 if [ ${lRCD} -eq 0 ]
                 then
                    echo "delete wls ${wlVers} OK"                             2>&1 | tee -a ${logFile}
                 else
                    echo "delete wls ${wlVers} with Error '${lRCD}'"           2>&1 | tee -a ${logFile}
                    cat /var/tmp/tmp.log | awk '{print (" | " $0) }'
                    exit ${lRCD}
                 fi 
                 cd $MW_HOME
                 echo "changed into Directory `pwd`"                           2>&1 | tee -a ${logFile}                                                                                                             
                 echo "remove all files"                                       2>&1 | tee -a ${logFile}                                                                                                             
                 rm -rf *                                                      2>&1 | tee -a ${logFile} 
                 wlsInstall="Y"
             fi
          else wlsInstall="Y"
          fi
          echo "wlsInstall set to '${wlsInstall}'"
          
          if [[ "${wlsInstall}" == "Y" ]]; then
              cd ${scriptDir}
              echo "changed into Directory `pwd`"                              2>&1 | tee -a ${logFile}
              if [[ "$prefix" == "w0"[1-9] ]]; then
                  INSTALL_TYPE="Fusion Middleware Infrastructure With Examples"
              else
                  INSTALL_TYPE="Fusion Middleware Infrastructure"
              fi
              export INSTALL_TYPE
              echo "INSTALL_TYPE '${INSTALL_TYPE}'"                            2>&1 | tee -a ${logFile}              
              envsubst < wlsconfig/create/response-file_template.rsp > response-file.rsp
              export PATH=$JAVA_HOME/bin:$PATH
              echo "install new fmw ${fmwVersion}"                             2>&1 | tee -a ${logFile}
              echo "Cmd: java -jar /u01/common/general/wls/fmw_${fmwVersion}_infrastructure.jar -silent -responseFile ${scriptDir}/response-file.rsp -jreLoc ${JAVA_HOME} -invPtrLoc ${scriptDir}/wlsconfig/create/oraInst.loc" >>${logFile}
              java -jar /u01/common/general/wls/fmw_${fmwVersion}_infrastructure.jar -silent -responseFile ${scriptDir}/response-file.rsp -jreLoc ${JAVA_HOME} -invPtrLoc ${scriptDir}/wlsconfig/create/oraInst.loc 2>&1 >/var/tmp/tmp.log
              lRCD=$?
              cat /var/tmp/tmp.log | awk '{print (" | " $0) }' >>${logFile}
              if [ ${lRCD} -eq 0 ]
              then
                 echo "Install fmw ${fmwVersion} OK"                           2>&1 | tee -a ${logFile}
              else
                 echo "Install fmw ${fmwVersion} with Error '${lRCD}'"         2>&1 | tee -a ${logFile}
                 cat /var/tmp/tmp.log | awk '{print (" | " $0) }'
                 exit ${lRCD}
              fi 
          fi
          
          eTaxProvider=`grep DOM ${configFile} | grep etax | wc -l`
          
          if [ ${eTaxProvider} != 0 ]; then
              cp -p ${originScriptDir}/wlsconfig/create/EtaxesTaxPayerAuthenticationProvider.jar ${MW_HOME}/wlserver/server/lib/mbeantypes/.
              echo "eTaxProvider.jar copied"                                   2>&1 | tee -a ${logFile}
          fi