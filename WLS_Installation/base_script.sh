#!/bin/bash
# Installation and operation of XPlan Weblogic

## Installation
### create folders




#verzeichnisstruktur erstellen (als oracle):

mkdir -m 755 /u01
mkdir -m 755 /u01/app
mkdir -m 755 /u01/common
mkdir -m 755 /u01/home
mkdir -m 755 /u01/app/oracle
mkdir -m 755 /u01/app/oraInventory
mkdir -m 755 $ORACLE_BASE/product
mkdir -m 755 $ORACLE_BASE/domains
mkdir -m 755 $ORACLE_BASE/nodemanager
mkdir -m 755 $ORACLE_BASE/logs
mkdir -m 755 $ORACLE_BASE/deployments
mkdir -m 755 $ORACLE_BASE/otdinst
mkdir -m 755 $COMMON_DIR/general
mkdir -m 755 $COMMON_DIR/images
mkdir -m 755 $COMMON_DIR/patches
mkdir -m 755 $DOMAIN_DIR/${DOMAIN_NAME}
mkdir -m 755 $NODEMANAGER_DIR/ndmgr_${DOMAIN_PREFIX}
mkdir -m 755 $LOGFILE_DIR/${DOMAIN_NAME}
mkdir -m 755 $DEPLOYMENT_DIR/${DOMAIN_NAME}
mkdir -m 755 $PRODUCT_INSTALL_DIR/${JDK_VERSION}
mkdir -m 755 $PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}
mkdir -m 750 $PRODUCT_INSTALL_DIR/${WL_VERSION}
mkdir -m 750 $PRODUCT_INSTALL_DIR/${WL_VERSION}/mwhome_${DOMAIN_PREFIX}
mkdir -m 755 $ORACLE_SOURCE
mkdir -m 755 /u01/app/oracle/resources
mkdir -p /u01/app/oracle/deployments/${DOMAIN_NAME}


### install JDK



echo ${JAVA_BIN_DIR}

cd ${JAVA_BIN_DIR}

echo ${JAVA_BIN_DIR}
pwd

mkdir $PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}
ls $PRODUCT_INSTALL_DIR/${JDK_VERSION}
cp -r $JAVA_BIN_DIR/* $PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}
echo $PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}
cd $PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}
chmod +rx -R ./*


echo "starting with installation"
### Install Weblogic


cd $WL_BIN_DIR
$PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}/bin/java -jar ./wls.jar -silent -responseFile ${scriptDir}/response-file.rsp


#${WL_HOME}/oracle_common/common/bin/config.sh  -mode=silent -silent_script=${scriptDir}/domain_template_generated.rsp


echo "finished installation"

#ssh localhost  "/bin/bash -c \"cd /u01/app/oracle/domains/${DOMAIN_NAME} && . ./bin/setDomainEnv.sh && ./startWebLogic.sh\" & " &


#${WL_HOME}/oracle_common/common/bin/wlst.sh ${scriptDir}/createDomain_generated.py


# echo "${WL_HOME}/common/bin/wlst.sh ${scriptDir}/pk_individual_settings_generated.py"

# create jdbc data source
# ${WL_HOME}/oracle_common/common/bin/wlst.sh ${scriptDir}/Datasource/dscreate_generated.py

# set domain individual settings for pk 
#${WL_HOME}/oracle_common/common/bin/wlst.sh ${scriptDir}/some_companyname_individual_settings_generated.py


# create jms
# ${WL_HOME}/oracle_common/common/bin/wlst.sh ${scriptDir}/create_jms_generated.py

#sed -i '163i export MEM_ARGS=\"-Xms8G -Xmx8G -XX:MaxPermSize=512m\"' /u01/app/oracle/domains/${DOMAIN_NAME}/bin/startWebLogic.sh
