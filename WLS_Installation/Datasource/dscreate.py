connect(username='weblogic',     password='orcl3db4',     url='t3://${ADMIN_URL}:${ADMIN_PORT}')
edit()
startEdit()

cd('/')
cmo.createJDBCSystemResource('${DBUSER}')

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}')
cmo.setName('${DBUSER}')

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCDataSourceParams/${DBUSER}')
set('JNDINames',jarray.array([String('pkTxDataSource')], String))

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCDriverParams/${DBUSER}')
cmo.setUrl('jdbc:oracle:thin:@(DESCRIPTION=(FAILOVER=ON)(ENABLE=BROKEN)(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=dmwi02-vip.intern.some_companyname)(PORT=1530))(ADDRESS=(PROTOCOL=TCP)(HOST=dmwi01-vip.intern.some_companyname)(PORT=1530)))(CONNECT_DATA=(SERVICE_NAME=${DBSVC})))')
cmo.setDriverName('oracle.jdbc.OracleDriver')
cmo.setPassword('${DBPW}')

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCConnectionPoolParams/${DBUSER}')
cmo.setTestTableName('SQL SELECT 1 FROM DUAL\r\n\r\n\r\n')

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCDriverParams/${DBUSER}/Properties/${DBUSER}')
cmo.createProperty('user')

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCDriverParams/${DBUSER}/Properties/${DBUSER}/Properties/user')
cmo.setValue('${DBUSER}')

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCDataSourceParams/${DBUSER}')
cmo.setGlobalTransactionsProtocol('OnePhaseCommit')

cd('/SystemResources/${DBUSER}')
set('Targets',jarray.array([ObjectName('com.bea:Name=${ADMIN_NAME},Type=Server')], ObjectName))

activate()

startEdit()

cd('/JDBCSystemResources/${DBUSER}/JDBCResource/${DBUSER}/JDBCConnectionPoolParams/${DBUSER}')
cmo.setMaxCapacity(100)
cmo.setCapacityIncrement(1)
cmo.setStatementCacheType('LRU')
cmo.setStatementCacheSize(100)
cmo.setInitialCapacity(10)

activate()