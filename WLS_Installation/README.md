# Installation instructions, cleanup and hints

## Installation

the steps needed for installation are described here:

- clone/copy the project to the server where the software should be installed
- copy the provided software (e.g.`S:\BAI\Plattformen\Datenbanken\Oracle\Xplan\Installationsdateien Axenta`) to:  /u01/sw/some_companyname_linux/dist (including Patch!)
- copy the propertiesfiles located in folder properties to: /u01/sw/some_companyname_linux/dist
- edit help.path in axserver___custom.properties
- execute the file with parameters (e.g. `/u01/common/general/templates/test.sh pksgp01 PKSGProduktion01 PKSGP01_AdminServer wlsadmin-sg.xplan.test.some_companynameappl.ch 7501 sg.xplan.test.some_companynameappl.ch 6501 PKSGTRUN 5ph4FW10EsU6gx29q198 pk_test`)

post-installation steps:

- access weblogic console: (e.g.: http://lnxsrv65.some_companynameappl.ch:7501/console)
- check settings (datasource, channels, deployment, jms, provider)
- kill adminserver by killing process found by `ps -ef` (e.g.: `ps -ef | grep -i pksgp`)
- start Weblogic Manually (e.g.: `ssh localhost "/bin/bash -c \"cd /u01/app/oracle/domains/PKSGProduktion01 && . ./bin/setDomainEnv.sh && ./startWebLogic.sh\" &" &`)

## Cleanup

execute the following steps to clean up domain:

- kill adminserver by killing process found by `ps -ef` (e.g.: `ps -ef | grep -i pksgp`)
- remove domain directory (e.g.: `rm -rf  /u01/app/oracle/domains/PKSGProduktion01`)
- remove mw_home directory (e.g.: `rm -rf /u01/app/oracle/product/10.3.3/mwhome_pksgp01`)
- remove jdk directory (e.g.: `rm -rf /u01/app/oracle/product/1.6.023/jdk_pksgp01`)

## HINT

if you ever migrate to another Software/WL release/Host remind the following points:

- change the software directory paths in main.sh (JAVA/WL/WL_HOME/MW_HOME)
- change the hostname for the ssh connection (remind to create ssh equivalence)
- ALWAYS call the script fully qualified!

# SSL configuration

below we describe the creation and the installation of certificates into the domain.

## create certificates

### generate csr

```shell
cd /u01/app/oracle/domains/PKSGTest01/

. bin/setDomainEnv.sh

cd /u01/app/oracle/domains/PKSGTest01/security
keytool -genkey -alias xplan.test.some_companynameappl.ch -keyalg RSA -keystore /u01/app/oracle/product/10.3.3/mwhome_pksgt01/wlserver_10.3/server/lib/DemoIdentity.jks -keysize 2048 -dname "CN=xplan.test.some_companynameappl.ch, OU=Technik, O=some_companyname Verwaltungsrechenzentrum AG, L=St. Gallen, ST=SG, C=CH" -validity 3650
DemoIdentityKeyStorePassPhrase
keytool -certreq -alias xplan.test.some_companynameappl.ch -keystore /u01/app/oracle/product/10.3.3/mwhome_pksgt01/wlserver_10.3/server/lib/DemoIdentity.jks -file xplan.test.some_companynameappl.ch.csr
DemoIdentityKeyStorePassPhrase


keytool -import -alias "some_companyname Certification Authority 1" -keystore /u01/app/oracle/product/10.3.3/mwhome_pksgt01/wlserver_10.3/server/lib/DemoTrust.jks -file /u01/app/oracle/domains/PKSGTest01/security/some_companynameCA.cer
DemoTrustKeyStorePassPhrase

keytool -import -alias "some_companyname Certification Authority 1" -keystore /u01/app/oracle/product/10.3.3/mwhome_pksgt01/wlserver_10.3/server/lib/DemoIdentity.jks -file /u01/app/oracle/domains/PKSGTest01/security/some_companynameCA.cer
DemoTrustKeyStorePassPhrase

keytool -import -trustcacerts -alias xplan.test.some_companynameappl.ch -keystore /u01/app/oracle/product/10.3.3/mwhome_pksgt01/wlserver_10.3/server/lib/DemoIdentity.jks -file  xplan.test.some_companynameappl.ch_8450_some_companyname_Web_Server_DMZ.cer
DemoIdentityKeyStorePassPhrase

cd /u01/app/oracle/domains/PKSGAbnahme01/

. bin/setDomainEnv.sh

cd /u01/app/oracle/domains/PKSGAbnahme01/security

keytool -import -alias "some_companyname Certification Authority 1" -keystore /u01/app/oracle/product/10.3.3/mwhome_pksga01/wlserver_10.3/server/lib/DemoTrust.jks -file /u01/app/oracle/domains/PKSGAbnahme01/security/some_companynameCA.cer
DemoTrustKeyStorePassPhrase

keytool -import -alias "some_companyname Certification Authority 1" -keystore /u01/app/oracle/product/10.3.3/mwhome_pksga01/wlserver_10.3/server/lib/DemoIdentity.jks -file /u01/app/oracle/domains/PKSGAbnahme01/security/some_companynameCA.cer
DemoIdentityKeyStorePassPhrase

keytool -import -trustcacerts -alias xplan.abnahme.some_companynameappl.ch -keystore /u01/app/oracle/product/10.3.3/mwhome_pksga01/wlserver_10.3/server/lib/DemoIdentity.jks -file   xplan.abnahme.some_companynameappl.ch_8449_some_companyname_Web_Server_DMZ.cer
DemoIdentityKeyStorePassPhrase


/u01/app/oracle/product/10.3.3/mwhome_pksgt01/wlserver_10.3/common/bin/wlst.sh


connect(username='weblogic',     password='orcl3db4',     url='t3://wlsadmin-sg.xplan.abnahme.some_companynameappl.ch:7201')
edit()
startEdit()

cd('/Servers/PKSGA01_AdminServer')
cmo.createNetworkAccessPoint('T3sClientNetwork')

cd('/Servers/PKSGA01_AdminServer/NetworkAccessPoints/T3sClientNetwork')
cmo.setProtocol('t3s')
cmo.setListenAddress('sg.xplan.abnahme.some_companynameappl.ch')
cmo.setListenPort(8101)
cmo.setPublicAddress('sg.xplan.abnahme.some_companynameappl.ch')
cmo.setEnabled(true)
cmo.setHttpEnabledForThisProtocol(true)
cmo.setTunnelingEnabled(false)
cmo.setOutboundEnabled(false)
cmo.setTwoWaySSLEnabled(false)
cmo.setClientCertificateEnforced(false)

activate()

startEdit()

cd('/Servers/PKSGA01_AdminServer/SSL/PKSGA01_AdminServer')
cmo.setHostnameVerificationIgnored(true)
cmo.setHostnameVerifier(None)
cmo.setTwoWaySSLEnabled(false)
cmo.setClientCertificateEnforced(false)

activate()

edit()
startEdit()

cd('/Servers/PKSGA01_AdminServer/NetworkAccessPoints/T3sClientNetwork')
cmo.setCustomPrivateKeyAlias('sg.xplan.abnahme.some_companynameappl.ch')
cmo.CustomPrivateKeyPassPhrase('DemoIdentityKeyStorePassPhrase')


```