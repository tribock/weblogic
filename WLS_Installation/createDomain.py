# Modify these values as necessary
import sys, traceback
import socket;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

scriptName=createDomain
print 'Set Properties';


java_home='${JAVA_HOME}';
wlHome='/u01/app/oracle/product/${WL_VERSION}/mwhome_${DOMAIN_PREFIX}';
templateHome=wlHome+'/wlserver/common/templates/wls';
domainTemplate=templateHome+'/wls.jar';
domainsHome='/u01/app/oracle/domains';
ttDomainName='${DOMAIN_NAME}';
ttDomainHome=domainsHome+'/'+ttDomainName;
server1Address='${ADMIN_URL}'
adminServerName='${ADMIN_NAME}';
adminListenPort=${ADMIN_PORT};
adminListenAddress='${ADMIN_URL}'
ttMachineName='TTMachine_01';
adminJavaArgsBase='-XX:PermSize=256m -XX:MaxPermSize=512mdf -Xms1024m -Xmx1532m'
logsHome='/u01/app/oracle/logs/'+ttDomainName
adminUser='weblogic';
adminPwd='orcl3db4';
nmHome=ttDomainHome+'nodemanager';
nmHost='cn01-priv';
nmPort='5556';
nmType='plain';


#
#Home Folders
wlsHome=wlHome+'/wlserver'
soaDomainHome=ttDomainHome
#
# Templates for 12.2.1
wlsjar=domainTemplate
oracleCommonTplHome=wlHome+'/oracle_common/common/templates'
#


# Determine the Server Java Args
def getServerJavaArgs(serverName,javaArgsBase,logsHome):
  javaArgs = javaArgsBase+' -Dweblogic.Stdout='+logsHome+'/'+serverName+'.out -Dweblogic.Stderr='+logsHome+'/'+serverName+'_err.out'
  return javaArgs


# Create a Unix Machine
def createUnixMachine(serverMachine,serverAddress):
  print('\nCreate machine '+serverMachine+' with type UnixMachine')
  
  cd('/')
  create(serverMachine,'UnixMachine')
  cd('UnixMachine/'+serverMachine)
  create(serverMachine,'NodeManager')
  cd('NodeManager/'+serverMachine)
  set('ListenAddress',serverAddress)


#
# Change Admin Server
def changeAdminServer(adminServerName,listenAddress,listenPort,javaArguments):
  print '\nChange AdminServer'

  cd('/Servers/AdminServer')
  # name of adminserver
  print '. Set Name to '+ adminServerName
  set('Name',adminServerName )
  cd('/Servers/'+adminServerName)
  # address and port
  print '. Set ListenAddress to '+ server1Address
  set('ListenAddress',server1Address)
  print '. Set ListenPort to '+ str(listenPort)
  set('ListenPort'   ,int(listenPort))
  #
  # ServerStart
  print 'Create ServerStart'
  create(adminServerName,'ServerStart')
  #cd('ServerStart/'+adminServerName)
  #print '. Set Arguments to: '+javaArguments
  #set('Arguments' , javaArguments)
  # SSL
  cd('/Server/'+adminServerName)
  print 'Create SSL'
  create(adminServerName,'SSL')
  cd('SSL/'+adminServerName)
  set('Enabled'                    , 'False')
  set('HostNameVerificationIgnored', 'True')
#
#
def main():
  try:
    #
    # Section 1: Base Domain + Admin Server

    print ('1. Create Base domain '+ttDomainName)
    print('\nCreate base wls domain with template '+wlsjar)

    readTemplate(wlsjar)
    #
    cd('/')
    # Domain Log
    print('Set base_domain log')
    create('base_domain','Log')  
    #
    # Admin Server
    setOption('NodeManagerHome', nmHome);
    setOption('DomainName', ttDomainName);
    setOption('OverwriteDomain', 'true');
    setOption('JavaHome', java_home);
    adminJavaArgs = getServerJavaArgs(adminServerName,adminJavaArgsBase,logsHome)
    changeAdminServer(adminServerName,adminListenAddress,adminListenPort,adminJavaArgs)  
    #
    print('\nSet password in '+'/Security/base_domain/User/weblogic')
    cd('/')
    cd('Security/base_domain/User/weblogic')
    # weblogic user name + password
    print('. Set Name to: ' +adminUser)
    set('Name',adminUser)
    cmo.setPassword(adminPwd)
    #

    print('. Set ServerStartMode to: ' +'prod')
    setOption('ServerStartMode', 'prod')

    #
    print('write Domain...')
    # write path + domain name
    writeDomain(soaDomainHome)
    closeTemplate()
    #
    #createAdminStartupPropertiesFile(soaDomainHome+'/servers/'+adminServerName+'/data/nodemanager',adminJavaArgs)
    #
    es = encrypt(adminPwd,soaDomainHome)
    #
    readDomain(soaDomainHome)
    #
    print('set Domain password for '+ttDomainName) 
    cd('/SecurityConfiguration/'+ttDomainName)
    set('CredentialEncrypted',es)
    #
    print('Set nodemanager password')
    set('NodeManagerUsername'         ,adminUser )
    set('NodeManagerPasswordEncrypted',es )

    # set NDMGR-Home
    cd('/')
    cd('NMProperties')
    set('NodeManagerHome', nmHome)
    #
    save()
    updateDomain()
    closeDomain();
    print ('\nFinished')
    #
    print('\nExiting...')
    exit()
   
  except NameError, e:
    print 'Apparently properties not set.'
    print "Please check the property: ", sys.exc_info()[0], sys.exc_info()[1]
  except:
    apply(traceback.print_exception, sys.exc_info())
    stopEdit('y')
    exit(exitcode=1)
#call main()
main()
exit()