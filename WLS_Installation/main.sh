#!/bin/bash

# example call: /u01/common/general/wls/PK_WLS_Installation/main.sh w01 Work01 5568


# check amount of args
if [ "$#" -ne 3 ]; then
  echo "Usage: $0 DOMAIN_PREFIX DOMAIN_NAME NODEMANAGER_PORT" >&2
  exit 1
fi

# set variables
DOMAIN_PREFIX=${1}
DOMAIN_NAME=${2}
NMPort=${3}


# set Version
export JDK_VERSION=1.8.101
export WL_VERSION=12.2.1

#until no AdminServerName fix found:
#ADMIN_NAME="AdminServer"

export DOMAIN_PREFIX=${DOMAIN_PREFIX}
export DOMAIN_NAME=${DOMAIN_NAME}


scriptDir=$(dirname "$0")
export scriptDir=${scriptDir}

ORACLE_BASE=/u01/app/oracle
export ORACLE_BASE

COMMON_DIR=/u01/common
export COMMON_DIR

PRODUCT_INSTALL_DIR=/u01/app/oracle/product
export PRODUCT_INSTALL_DIR

DOMAIN_DIR=/u01/app/oracle/domains
export DOMAIN_DIR

NODEMANAGER_DIR=/u01/app/oracle/nodemanager
export NODEMANAGER_DIR

LOGFILE_DIR=/u01/app/oracle/logs
export LOGFILE_DIR

DEPLOYMENT_DIR=/u01/app/oracle/deployments
export DEPLOYMENT_DIR

JAVA_HOME=$PRODUCT_INSTALL_DIR/${JDK_VERSION}/jdk_${DOMAIN_PREFIX}
export JAVA_HOME


WL_HOME=$PRODUCT_INSTALL_DIR/${WL_VERSION}/mwhome_${DOMAIN_PREFIX}
export WL_HOME

MW_HOME=$PRODUCT_INSTALL_DIR/${WL_VERSION}/mwhome_${DOMAIN_PREFIX}
export MW_HOME

export WL_BIN_DIR=${scriptDir}/bin/

echo ${scriptDir}
export JAVA_BIN_DIR=$scriptDir/bin/java

ORACLE_SOURCE=/orasrc
export ORACLE_SOURCE


# add java home to path
PATH=$JAVA_HOME/bin:$PATH; export PATH
envsubst < ${scriptDir}/base_script.sh > ${scriptDir}/install.sh
envsubst < ${scriptDir}/mw_install.xml > ${scriptDir}/mw_install_generated.xml
envsubst < ${scriptDir}/domain_template.rsp > ${scriptDir}/domain_template_generated.rsp
envsubst < ${scriptDir}/response-file_template.rsp > ${scriptDir}/response-file.rsp


envsubst <  ${scriptDir}/createDomain.py > ${scriptDir}/createDomain_generated.py

envsubst < ${scriptDir}/some_companyname_individual_settings.py > ${scriptDir}/some_companyname_individual_settings_generated.py


chmod -R +rx ${scriptDir}/*
${scriptDir}/install.sh

#chmod +rx /u01/app/oracle/domains/${DOMAIN_NAME}/bin/startWebLogic.sh

echo "run the following command after creating the domainvalues entry in https://git.workshop21.ch/middleware/weblogic_scripts/blob/master/domainValues.inc:"
echo "wlsconfig ${DOMAIN_PREFIX} createDomain ${WL_HOME}/oracle_common/common/bin/wlst.sh ${NMPort}"
