connect(username='weblogic',     password='orcl3db4',     url='t3://${ADMIN_URL}:${ADMIN_PORT}')
edit()
startEdit()

cd('/JTA/${DOMAIN_NAME}')
cmo.setMaxTransactions(10000)
cmo.setUnregisterResourceGracePeriod(30)
cmo.setAbandonTimeoutSeconds(86400)
cmo.setTimeoutSeconds(600)
cmo.setForgetHeuristics(true)
cmo.setBeforeCompletionIterationLimit(10)
cmo.setCheckpointIntervalSeconds(300)
cmo.setMaxUniqueNameStatistics(1000)



cd('/')
cmo.setClusterConstraintsEnabled(false)
cmo.setGuardianEnabled(false)
cmo.setAdministrationPortEnabled(false)
cmo.setConsoleEnabled(true)
cmo.setConsoleExtensionDirectory('console-ext')
cmo.setAdministrationProtocol('t3s')
cmo.setConfigBackupEnabled(false)
cmo.setConfigurationAuditType('none')
cmo.setInternalAppsDeployOnDemandEnabled(false)
cmo.setConsoleContextPath('console')

cd('/JMX/${DOMAIN_NAME}')
cmo.setManagementEJBEnabled(true)
cmo.setPlatformMBeanServerEnabled(false)
cmo.setPlatformMBeanServerUsed(true)
cmo.setCompatibilityMBeanServerEnabled(true)
cmo.setInvocationTimeoutSeconds(0)

cd('/DeploymentConfiguration/${DOMAIN_NAME}')
cmo.setRemoteDeployerEJBEnabled(false)

cd('/AdminConsole/${DOMAIN_NAME}')
cmo.setCookieName('ADMINCONSOLESESSION_${DOMAIN_NAME}')
cmo.setSessionTimeout(3600)



cd('/Servers/${ADMIN_NAME}/SSL/${ADMIN_NAME}')
cmo.setExportKeyLifespan(500)
cmo.setUseServerCerts(false)
cmo.setSSLRejectionLoggingEnabled(true)
cmo.setAllowUnencryptedNullCipher(false)
cmo.setInboundCertificateValidation('BuiltinSSLValidationOnly')
cmo.setOutboundCertificateValidation('BuiltinSSLValidationOnly')


cmo.setHostnameVerificationIgnored(false)
cmo.setHostnameVerifier(None)
cmo.setTwoWaySSLEnabled(false)
cmo.setClientCertificateEnforced(false)
cmo.setJSSEEnabled(true)



cd('/Servers/${ADMIN_NAME}')
cmo.setIdleConnectionTimeout(65)
cmo.setTunnelingClientTimeoutSecs(40)
cmo.setTunnelingEnabled(false)
cmo.setTunnelingClientPingSecs(45)
cmo.setMaxMessageSize(10000000)
cmo.setCompleteMessageTimeout(480)


cmo.setMaxMessageSize(100000000)



cd('/Servers/${ADMIN_NAME}/Log/${ADMIN_NAME}')
cmo.setLogFileRotationDir('/u01/app/oracle/logs/${DOMAIN_NAME}/${ADMIN_NAME}')
cmo.setRotationType('bySize')
cmo.setNumberOfFilesLimited(false)
cmo.setFileMinSize(5000)
cmo.setRotateLogOnStartup(true)
cmo.setFileName('/u01/app/oracle/logs/${DOMAIN_NAME}/${ADMIN_NAME}/${ADMIN_NAME}.log')


cmo.setStacktraceDepth(5)
cmo.setDomainLogBroadcasterBufferSize(10)
cmo.setLog4jLoggingEnabled(false)
cmo.setDateFormatPattern('MMM d, yyyy h:mm:ss a z')
cmo.setBufferSizeKB(8)
cmo.setLoggerSeverity('Info')
cmo.setServerLoggingBridgeUseParentLoggersEnabled(false)
cmo.setDomainLogBroadcastSeverity('Notice')
cmo.setRedirectStderrToServerLogEnabled(true)
cmo.setStdoutLogStack(true)
cmo.setLogFileSeverity('Notice')
cmo.setMemoryBufferSize(500)
cmo.setStdoutFormat('standard')
cmo.setRedirectStdoutToServerLogEnabled(true)
cmo.setStdoutSeverity('Notice')
cmo.setMemoryBufferSeverity('Debug')

activate()

startEdit()

cd('/SecurityConfiguration/${DOMAIN_NAME}/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
cmo.setControlFlag('SUFFICIENT')

activate()

startEdit()

cd('/SecurityConfiguration/${DOMAIN_NAME}/Realms/myrealm/UserLockoutManager/UserLockoutManager')

cmo.setLockoutEnabled(false)

activate()


startEdit()

cd('/WebAppContainer/${DOMAIN_NAME}')
cmo.setWorkContextPropagationEnabled(true)
cmo.setOptimisticSerialization(false)
cmo.setWAPEnabled(false)
cmo.setPostTimeoutSecs(30)
cmo.setMaxPostSize(-1)
cmo.setOverloadProtectionEnabled(false)
cmo.setClientCertProxyEnabled(false)
cmo.setJSPCompilerBackwardsCompatible(false)
cmo.setShowArchivedRealPathEnabled(false)
cmo.setHttpTraceSupportEnabled(false)
cmo.setMimeMappingFile('./config/mimemappings.properties')
cmo.setReloginEnabled(false)
cmo.setWeblogicPluginEnabled(false)
cmo.setXPoweredByHeaderLevel('NONE')
cmo.setRtexprvalueJspParamName(false)
cmo.setAllowAllRoles(false)
cmo.setAuthCookieEnabled(true)
cmo.setMaxPostTimeSecs(-1)
cmo.setFilterDispatchedRequestsEnabled(false)
cmo.setXPoweredByHeaderLevel('NONE')

activate()

